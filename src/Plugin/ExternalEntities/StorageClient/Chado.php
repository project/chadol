<?php

namespace Drupal\chadol\Plugin\ExternalEntities\StorageClient;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Database as CoreDatabase;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\dbxschema\Database\DatabaseTool;
use Drupal\external_entities\Form\ExternalEntityTypeForm;
use Drupal\xnttsql\Plugin\ExternalEntities\StorageClient\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * External entities storage client for Chado schema.
 *
 * @StorageClient(
 *   id = "xnttchado",
 *   label = @Translation("Chado"),
 *   description = @Translation("Retrieves Chado records. Chado is a PostgreSQL database schema designed for biological/genomics data.")
 * )
 */
class Chado extends Database {

  /**
   * Name or uniquename to use when empty.
   */
  const NONAME = '-';

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Managed Chado external entity fields.
   *
   * Keys are qualified Chado table aliases and values are associated field
   * settings.
   *
   * @var array
   */
  protected $chadoFields;

  /**
   * Placeholder values used by Chado filters.
   *
   * Keys are placeholder names and values are their associated values.
   *
   * @var array
   */
  protected $chadoFilterValuePlaceholders;

  /**
   * Constructs a database external storage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend service.
   * @param \Drupal\dbxschema\Database\DatabaseTool $database_tool
   *   Database tool service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    MessengerInterface $messenger,
    CacheBackendInterface $cache,
    ?DatabaseTool $database_tool,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_type_manager,
      $entity_field_manager,
      $messenger,
      $cache,
      $database_tool,
    );

    // Services injection.
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $messenger = $container->get('messenger');
    try {
      $db_tool = $container->get('dbxschema.tool');
    }
    catch (DatabaseToolException | PluginException $e) {
      // DatabaseToolException: Invalid argument passed to DBTool.
      // PluginException: Driver not found.
      $messenger->addError(
        'CHADOL: ' . $e->getMessage()
      );
    }

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $messenger,
      $container->get('cache.default'),
      $db_tool,
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config = NestedArray::mergeDeep(
      [
        'chado_config' => [
          'datatype' => [
            'mode' => '',
            'chado_type' => '',
            'chado_table' => '',
          ],
          'fields' => [],
          'joins' => ['list' => []],
        ],
      ],
      $config
    );
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $sc_id = ($form['#attributes']['id'] ??= uniqid('sc', TRUE));

    // Limit to 1 schema.
    $form_state->set('schema_count', 1);

    // Use xnttdb form as basis.
    $form = parent::buildConfigurationForm($form, $form_state);

    // Modify some parent elements.
    $form['connection']['#title'] = $this->t('Chado connection settings');
    $form['connection']['#open'] = TRUE;

    // @todo Use self::overrideForm.
    // Remove unnecessary parent elements.
    unset($form['queries']);
    unset($form['placeholder_settings']);
    unset($form['filter_mappings']);
    unset($form['connection']['dbkey']);
    unset($form['connection']['schemas']);
    unset($form['connection']['description']);
    unset($form['connection']['add_schema']);

    // Add new elements.
    $form['#attached']['library'][] = 'chadol/global-styling';
    $all_chado_instances = $this->getChadoInstances();
    if (empty($all_chado_instances)) {
      $form['connection']['chado_none'] = [
        '#type' => 'markup',
        '#markup' => $this->t(
          'No Chado instance found. Make sure the Drupal database account has access to local Chado schemas or set new Chado database credentials in your site settings.php. See documentation for details.'
        ),
      ];
      // No Chado connection found, stop here.
      return $form;
    }

    $chado_instance = $form_state->get('chado_instance');
    if (!empty($chado_instance)) {
      // Load new config from form update submission.
      [$dbkey, $schema] = explode('#', $chado_instance);
      $current_config = $this->getConfiguration();
      $current_config['connection'] = [
        'dbkey' => $dbkey,
        'schemas' => [$schema],
      ];
      $this->setConfiguration($current_config);
    }
    elseif (!empty($this->configuration['connection']['schemas'][0])) {
      // Try to load from config.
      $chado_instance =
        $this->configuration['connection']['dbkey']
        . '#'
        . $this->configuration['connection']['schemas'][0];
    }

    $form['connection']['chado_instance'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Select a chado database'),
      '#options' => $all_chado_instances,
      '#default_value' => $chado_instance,
      '#weight' => 10,
    ];

    // Database selection.
    // @todo Add state management: on change, hide the rest of the form.
    $form['connection']['submit_schema'] = [
      '#type' => 'submit',
      '#value' => $this->t('Use this Chado instance'),
      '#name' => 'submit_schema',
      '#weight' => 20,
      '#ajax' => [
        'callback' => [
          get_class($this),
          'buildAjaxParentSubForm',
        ],
        'wrapper' => $form['#attributes']['id'],
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    if (empty($chado_instance)) {
      // No Chado connection set yet, stop here.
      return $form;
    }

    // Wrapper for the rest.
    $chado_config = $this->configuration['chado_config'] ?? NULL;
    $form['chado_config'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $sc_id . 'cc',
      ],
      '#weight' => 40,
    ];
    // Select a data type.
    $form['chado_config']['datatype'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Data type'),
    ];
    $mode_id = uniqid('mode', TRUE);
    $form['chado_config']['datatype']['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose by'),
      '#options' => [
        'type_id' => $this->t('Data type'),
        'table' => $this->t('Chado table'),
      ],
      '#attributes' => [
        'data-chadol-selector' => $mode_id,
      ],
      '#default_value' => $this->configuration['chado_config']['datatype']['mode'] ?? 'type_id',
    ];

    $this->initConnection($this->configuration);

    // List table types.
    $form['chado_config']['datatype']['chado_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a data type'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['chado_config']['datatype']['chado_type'] ?? NULL,
      '#options' => $this->getChadoDataTypes(),
      '#sort_options' => TRUE,
      '#states' => [
        'visible' => [
          'input[data-chadol-selector="' . $mode_id . '"]' => ['value' => 'type_id'],
        ],
      ],
    ];

    // List main chado tables (with _id, type_id).
    $form['chado_config']['datatype']['chado_table'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a Chado table'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['chado_config']['datatype']['chado_table'] ?? NULL,
      '#options' => $this->getChadoTables(),
      '#sort_options' => TRUE,
      '#states' => [
        'visible' => [
          'input[data-chadol-selector="' . $mode_id . '"]' => ['value' => 'table'],
        ],
      ],
    ];

    $form['chado_config']['datatype']['select'] = [
      '#type' => 'submit',
      '#value' => $this->t('Set data type'),
      '#name' => 'submit_datatype',
      '#ajax' => [
        'callback' => [
          get_class($this),
          'buildAjaxParentSubForm',
        ],
        'wrapper' => $form['chado_config']['#attributes']['id'],
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    // Check if we got a data type.
    $table = $form_state->get(
      'datatype_table',
       NULL
    );
    $type_id = NULL;
    if (empty($table) && !empty($chado_config['datatype']['mode'])) {
      // Not from a previous form submission, check from config.
      if ('table' == $chado_config['datatype']['mode']) {
        $table = $chado_config['datatype']['chado_table'] ?? NULL;
      }
      elseif ('type_id' == $chado_config['datatype']['mode']) {
        $chado_type = $chado_config['datatype']['chado_type'] ?? '';
        if (preg_match('/(.+)_(\d+)/', $chado_type, $match)) {
          $table = $match[1];
          $type_id = $match[2];
        }
      }
    }
    if ($table) {
      // Adds table fields selection and filtering.
      $form['chado_config']['fields'] = $this->getChadoTableFieldForm(
        $table,
        $form_state,
        [
          'type_id' => $type_id,
          'values' => $this->configuration['chado_config']['fields'],
        ]
      );

      // Adds data joins selection and filtering.
      $form['chado_config']['joins'] = $this->getChadoDataJoinForm(
        $table,
        $form_state,
        $form['chado_config']['#attributes']['id']
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    // Generate SQL queries to have them validated by parent class.
    // Initialize form values.
    $connection = ['dbkey' => '', 'schemas' => []];
    $queries = [
      'create' => [''],
      'read' => ['SELECT 0 WHERE 0 == :id'],
      'update' => [''],
      'delete' => [''],
      'list' => ['SELECT 0 WHERE FALSE :filters'],
      'count' => ['SELECT 0 AS "count" WHERE TRUE :filters'],
    ];
    $placeholders = ['placeholders' => []];
    $filter_mappings = [];

    // Initialize connection.
    $chado_instance = $form_state->getValue(['connection', 'chado_instance']) ?? '#';
    [$dbkey, $schema] = explode('#', $chado_instance);
    $connection['dbkey'] = $dbkey;
    $connection['schemas'][0] = $schema;

    $form_state->setValue('connection', $connection);
    $form_state->setValue('queries', $queries);
    $form_state->setValue('placeholder_settings', $placeholders);
    $form_state->setValue('filter_mappings', $filter_mappings);

    // Get new Chado config.
    $chado_config = $form_state->getValue('chado_config', []);
    $chado_config = NestedArray::mergeDeep(
      [
        'datatype' => ['mode' => ''],
        'fields' => [],
        'joins' => ['list' => []],
      ],
      $chado_config
    );

    // Cleanup unnecessay values.
    unset($chado_config['datatype']['select']);
    unset($chado_config['add_join']);
    // Cleanup joins.
    foreach ($chado_config['joins']['list'] as $i => $join) {
      if (empty($join['join_name']) || ('remove' === $join['join_type'])) {
        unset($chado_config['joins']['list'][$i]);
      }
    }
    $form_state->setValue('chado_config', $chado_config);
    $form_state->set('join_count', count($chado_config['joins']['list']));

    // We need to update config here and now for connection in order to complete
    // other config elements after.
    $this->setConfiguration($form_state->getValues());

    // Check for Ajax events.
    if ($trigger = $form_state->getTriggeringElement()) {
      switch ($trigger['#name']) {

        case 'submit_schema':
          $form_state->set('chado_instance', $chado_instance);
          $this->clearChadoConfiguration($form_state);
          $form_state->setRebuild(TRUE);
          break;

        case 'submit_datatype':
          if ('table' == $chado_config['datatype']['mode']) {
            $table = $chado_config['datatype']['chado_table'];
          }
          elseif ('type_id' == $chado_config['datatype']['mode']) {
            $chado_type = $chado_config['datatype']['chado_type'];
            if (preg_match('/(.+)_(\d+)/', $chado_type, $match)) {
              $table = $match[1];
              $type_id = $match[2];
            }
          }
          $form_state->set('datatype_table', $table);
          $this->clearChadoConfiguration($form_state);
          $form_state->setRebuild(TRUE);
          break;

        case 'add_join':
          $join_count = $form_state->get('join_count');
          $form_state->set('join_count', $join_count + 1);
          $form_state->setRebuild(TRUE);
          break;

        default:
      }
    }

    // Process form values.
    if (!empty($this->xConnection)) {
      $queries = $this->generateSqlQueries($chado_config);

      // Update placeholders.
      if (!empty($this->chadoFilterValuePlaceholders)) {
        foreach ($this->chadoFilterValuePlaceholders as $placeholder => $value) {
          $placeholders['placeholders'][] = [
            'placeholder' => $placeholder,
            'constant' => $value,
          ];
        }
      }
    }

    $form_state->setValue('queries', $queries);
    $form_state->setValue('placeholder_settings', $placeholders);
    $form_state->setValue('filter_mappings', $filter_mappings);
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * Generate SQL queries.
   *
   * @param array $chado_config
   *   A Chado config array.
   *
   * @return array
   *   An array of queries with the following structure:
   *   @code
   *   [
   *     'create' => [<string>],
   *     'read'   => [<string>],
   *     'update' => [<string>],
   *     'delete' => [<string>],
   *     'list'   => [<string>],
   *     'count'  => [<string>],
   *   ];
   *   @endcode
   */
  public function generateSqlQueries(array $chado_config) :array {
    // Initialize query structure.
    $queries = [
      'create' => [''],
      'read' => ['SELECT 0 WHERE 0 == :id'],
      'update' => [''],
      'delete' => [''],
      'list' => ['SELECT 0 WHERE FALSE :filters'],
      'count' => ['SELECT 0 AS "count" WHERE TRUE :filters'],
    ];

    // Generate SQL queries with placeholders from Chado config.
    // Get Chado data mode.
    // IMPORTANT: The first element of $where clause array must be the entity
    // id filter.
    $where = [];
    if ('table' == $chado_config['datatype']['mode']) {
      $table = $chado_config['datatype']['chado_table'];
      $id_field = "t.${table}_id";
      $where[] = $id_field . ' IN (:id[])';
    }
    elseif ('type_id' == $chado_config['datatype']['mode']) {
      $chado_type = $chado_config['datatype']['chado_type'];
      if (preg_match('/(.+)_(\d+)/', $chado_type, $match)) {
        $table = $match[1];
        $id_field = "t.${table}_id";
        $where[] = $id_field . ' IN (:id[])';
        $where[] = 't.type_id = ' . $match[2];
      }
    }
    if (!empty($table)) {
      // Get table definition.
      $table_def = $this->getChadoTableDef($table);
      if (!empty($table_def)) {
        // Array of fields for the SELECT clause. Starts with all table
        // fields.
        $select = array_map(
          function ($x) {
            // Special case for name and uniquename: we don't want them empty.
            if (('name' === $x) || ('uniquename' == $x)) {
              return "coalesce(nullif(trim(t.$x),''),'"
              . static::NONAME
              . "') AS \"$x\"";
            }
            return "t.$x AS \"$x\"";
          },
          array_keys($table_def['fields'])
        );
        $group_by = array_map(
          function ($x) {
            return "t.$x";
          },
          array_keys($table_def['fields'])
        );
        $from = [["{1:$table} t"]];

        // Get field filters.
        $fields = $chado_config['fields'];
        foreach ($fields as $field => $field_settings) {
          // Remove field prefix.
          $field_name = preg_replace('/^' . ExternalEntityTypeForm::MANAGED_FIELD_PREFIX . '/', '', $field);
          switch ($field_settings['filter']['filter_type'] ?? '') {
            case 'string':
            case 'text':
              // Manage filtering.
              $where = array_merge(
                $where,
                $this->getTextFilterConditions(
                  $field_settings['filter'],
                  "t.$field_name"
                )
              );
              break;

            case 'boolean':
              // Manage filtering.
              $where = array_merge(
                $where,
                $this->getBooleanFilterConditions(
                  $field_settings['filter'],
                  "t.$field_name"
                )
              );
              break;

            case 'decimal':
            case 'float':
            case 'integer':
              // Manage filtering.
              $where = array_merge(
                $where,
                $this->getNumericFilterConditions(
                  $field_settings['filter'],
                  "t.$field_name"
                )
              );
              break;

            case 'datetime':
              // Manage filtering.
              $where = array_merge(
                $where,
                $this->getDateFilterConditions(
                  $field_settings['filter'],
                  "t.$field_name"
                )
              );
              break;

            case 'cvterm':
              // Manage filtering.
              $where = array_merge(
                $where,
                $this->getChadoCvtermFilterConditions(
                  $field_settings['filter'],
                  "t.$field_name"
                )
              );
              break;

            case 'dbxref':
              // Manage filtering.
              $where = array_merge(
                $where,
                $this->getChadoDbxrefFilterConditions(
                  $field_settings['filter'],
                  "t.$field_name"
                )
              );
              break;

            case 'organism':
              // Manage filtering.
              $where = array_merge(
                $where,
                $this->getChadoOrganismFilterConditions(
                  $field_settings['filter'],
                  "t.$field_name"
                )
              );
              break;

            default:
              $this->logger->warning(
                'Invalid table field type '
                . $field_settings['filter']['filter_type']
              );
              break;
          }
        }

        // Get joins.
        $joins = $chado_config['joins']['list'];
        foreach ($joins as $join_index => $join) {
          $join_name = 'j' . $join_index;
          $join_alias = strtolower(
            preg_replace('/\W/', '_', $join['join_name'])
          );

          if (empty($join['required'])) {
            $left = ' LEFT';
          }
          else {
            $left = '';
          }
          // Note: we use e'\\x5b\\x5d' instead of '[]' to prevent Drupal
          // replacement in SQL queries.
          switch ($join['join_type'] ?? '') {
            case 'prop':
              $conditions = $this->getChadoTablePropFilterConditions(
                $join['join_prop'],
                $join_name
              );
              if (empty($conditions)) {
                $sql_cond = '';
              }
              else {
                // Prepend with "AND" as we append the non-empy expression to
                // a "JOIN ... ON ...".
                $sql_cond = ' AND ' . implode(' AND ', $conditions);
              }
              $select[] =
                "array_agg($join_name.value ORDER BY $join_name.rank ASC) AS \"array_"
                . $join_alias
                . '"';
              $select[] =
                "COALESCE(json_object_agg($join_name.${table}prop_id, json_build_object('type_id', $join_name.type_id, 'value', $join_name.value, 'rank', $join_name.rank)) FILTER (WHERE $join_name.${table}prop_id IS NOT NULL), e'\\x5b\\x5d'::json) AS \"json_"
                . $join_alias
                . '"';
              $from[0][] = "$left JOIN {1:${table}prop} $join_name ON ($join_name.${table}_id = t.${table}_id $sql_cond)";
              break;

            case 'cvterm':
              $conditions = $this->getChadoTableCvtermFilterConditions($join['join_cvterm'], $join_name);
              if (empty($conditions)) {
                $sql_cond = '';
              }
              else {
                // Prepend with "AND" as we append the non-empy expression to
                // a "JOIN ... ON ...".
                $sql_cond = ' AND ' . implode(' AND ', $conditions);
              }
              $select[] =
                "array_agg(${join_name}cvt.name ORDER BY $join_name.rank ASC) AS \"array_"
                . $join_alias
                . '"';
              $select[] =
                "COALESCE(json_object_agg($join_name.${table}_cvterm_id, json_build_object('cvterm', ${join_name}cvt.name, 'cvterm_id', $join_name.cvterm_id, 'pub_id', $join_name.pub_id, 'is_not', $join_name.is_not, 'rank', $join_name.rank)) FILTER (WHERE $join_name.${table}_cvterm_id IS NOT NULL), e'\\x5b\\x5d'::json) AS \"json_"
                . $join_alias
                . '"';
              $from[0][] = "$left JOIN {1:${table}_cvterm} $join_name ON ($join_name.${table}_id = t.${table}_id $sql_cond)
                         LEFT JOIN {1:cvterm} ${join_name}cvt ON (${join_name}cvt.cvterm_id = ${join_name}.cvterm_id)";
              break;

            case 'dbxref':
              $conditions = $this->getChadoTableDbxrefFilterConditions(
                $join['join_cvterm'],
                $join_name
              );
              if (empty($conditions)) {
                $sql_cond = '';
              }
              else {
                // Prepend with "AND" as we append the non-empy expression to
                // a "JOIN ... ON ...".
                $sql_cond = ' AND ' . implode(' AND ', $conditions);
              }
              $select[] =
                "array_agg(${join_name}db.urlprefix || ${join_name}dbx.accession) AS \"array_"
                . $join_alias
                . '"';
              $select[] =
                "COALESCE(json_object_agg(${join_name}.${table}_dbxref_id, json_build_object('dbxref_id', $join_name.dbxref_id, 'url', ${join_name}db.urlprefix || ${join_name}dbx.accession, 'db', ${join_name}db.name, 'accession', ${join_name}dbx.accession, 'version', ${join_name}dbx.version, 'is_current', $join_name.is_current)) FILTER (WHERE ${join_name}.${table}_dbxref_id IS NOT NULL), e'\\x5b\\x5d'::json) AS \"json_"
                . $join_alias
                . '"';
              $from[0][] = "$left JOIN {1:${table}_dbxref} $join_name ON ($join_name.${table}_id = t.${table}_id $sql_cond)
                         LEFT JOIN {1:dbxref} ${join_name}dbx ON (${join_name}dbx.dbxref_id = ${join_name}.dbxref_id)
                         LEFT JOIN {1:db} ${join_name}db ON (${join_name}db.db_id = ${join_name}dbx.db_id)";
              break;

            case 'pub':
              $conditions = $this->getChadoTablePubFilterConditions($join['join_pub'], $join_name);
              if (empty($conditions)) {
                $sql_cond = '';
              }
              else {
                // Prepend with "AND" as we append the non-empy expression to
                // a "JOIN ... ON ...".
                $sql_cond = ' AND ' . implode(' AND ', $conditions);
              }
              $select[] =
                "array_agg(${join_name}.pub_id) AS \"array_"
                . $join_alias
                . '"';
              $select[] =
                "COALESCE(json_object_agg(${join_name}.${table}_pub_id, json_build_object('pub_id', $join_name.pub_id, 'title', ${join_name}pub.title, 'pyear', ${join_name}pub.pyear, 'uniquename', ${join_name}pub.uniquename, 'type_id', ${join_name}pub.type_id, 'miniref', ${join_name}pub.miniref)) FILTER (WHERE ${join_name}.${table}_pub_id IS NOT NULL), e'\\x5b\\x5d'::json) AS \"json_"
                . $join_alias
                . '"';
              $from[0][] = "$left JOIN {1:${table}_pub} $join_name ON ($join_name.${table}_id = t.${table}_id $sql_cond)
                         LEFT JOIN {1:pub} ${join_name}pub ON (${join_name}pub.pub_id = ${join_name}.pub_id)";
              break;

            case 'relationship':
              $conditions = $this->getChadoTableRelationshipFilterConditions(
                $join['join_rel'],
                $join_name,
                't.' . $table . '_id'
              );
              if (!empty($conditions)) {
                // Don't prepend an "AND" as we have no conditions to the
                // join.
                $sql_cond = implode(' AND ', $conditions);
                $select[] =
                  "COALESCE(json_object_agg(${join_name}.${table}_relationship_id, json_build_object('subject_id', $join_name.subject_id, 'object_id', $join_name.object_id, 'type_id', $join_name.type_id)) FILTER (WHERE ${join_name}.${table}_relationship_id IS NOT NULL), e'\\x5b\\x5d'::json) AS \"json_"
                  . $join_alias
                  . '"';
                $from[0][] = "$left JOIN {1:${table}_relationship} $join_name ON ($sql_cond)";
                // Check if subject or object.
                if ('subject' == $join['join_rel']['filter_mode']) {
                  $select[] =
                    "array_agg(${join_name}.object_id) AS \"array_"
                    . $join_alias
                    . '"';
                }
                elseif ('object' == $join['join_rel']['filter_mode']) {
                  $select[] =
                    "array_agg(${join_name}.subject_id) AS \"array_"
                    . $join_alias
                    . '"';
                }
                elseif ('any' == $join['join_rel']['filter_mode']) {
                  $select[] = "array_agg(DISTINCT CASE WHEN $join_name.subject_id = t.${table}_id THEN $join_name.object_id WHEN $join_name.object_id = t.${table}_id THEN $join_name.subject_id END) AS \"array_" . $join_alias . '"';
                }
                else {
                  $this->logger->warning(
                    'Invalid relationship join mode "' . ($join['join_rel']['filter_mode'] ?? '-undefined-') . '" for "' . $join['join_name'] . '"'
                  );
                }
              }
              else {
                // Warn for invalid settings.
                $this->logger->warning(
                  'Invalid relationship settings for "' . $join['join_name'] . '"'
                );
              }
              break;

            default:
              if ('fk__' == substr($join['join_type'], 0, 4)) {
                [, $join_table, $join_key, $other_table] = explode('__', $join['join_type']);
                // @todo Implement.
                if (empty($other_table)) {
                  // 1-to-many  join type.
                  // ex.: fk__featureloc__srcfeature_id
                  $select[] = "array_agg(DISTINCT $join_name.${join_table}_id) AS \"array_$join_alias\"";
                  $from[0][] = "$left JOIN {1:$join_table} $join_name ON ($join_name.$join_key = t.${table}_id)";
                }
                else {
                  // Many-to-many join type.
                  // ex.: fk__nd_experiment_stock__stock_id__nd_experiment.
                  $select[] = "array_agg(DISTINCT $join_name.${other_table}_id) AS \"array_$join_alias\"";
                  $from[0][] = "$left JOIN {1:$join_table} $join_name ON ($join_name.$join_key = t.${table}_id)";
                  // If we later need to join the other table to grab its
                  // data:
                  // phpcs:disable
                  // @code
                  // "$left JOIN {1:$join_table} $join_name ON ($join_name.$join_key = t.${table}_id)
                  //  $left JOIN {1:$other_table} ${join_name}x ON (${join_name}x.${other_table}_id = $join_name.${other_table}_id)".
                  // @endcode
                  // phpcs:enable
                }
                break;
              }

              $this->logger->warning(
                'Invalid join type: "' . $join['join_type'] . '"'
              );
              break;
          }
        }

        // Build final SQL queries.
        $final_select = implode(', ', $select);
        $final_from = substr(
          array_reduce(
            $from,
            function ($f, $t) {
              return $f . ', ' . implode("\n  ", $t);
            },
            ''
          ),
          // == strlen(', ') (used in function above, before "implode")
          2
        );
        $final_where = implode(' AND ', $where);
        $final_group_by = implode(', ', $group_by);
        // READ.
        $queries['read'] = [
          'SELECT '
          . $final_select
          . ' FROM '
          . $final_from
          . ' WHERE '
          . $final_where
          . ' GROUP BY '
          . $final_group_by,
        ];
        // LIST.
        $queries['list'] = [
          'SELECT '
          . $final_select
          . ' FROM '
          . $final_from,
        ];
        // COUNT.
        $queries['count'] = [
          'SELECT COUNT(DISTINCT t.'
          . $table
          . '_id) AS "count" FROM '
          . $final_from,
        ];
        // Append LIST/COUNT filters.
        if (1 < count($where)) {
          // Remove ID condition.
          $final_where_no_id = implode(' AND ', array_slice($where, 1));
          $queries['list'][0] .= ' WHERE '
            . $final_where_no_id
            . ' :filters GROUP BY '
            . $final_group_by;
          $queries['count'][0] .= ' WHERE '
            . $final_where_no_id
            . ' :filters';
        }
        else {
          $queries['list'][0] .=
            ' WHERE TRUE :filters GROUP BY '
            . $final_group_by;
          $queries['count'][0] .=
            ' WHERE TRUE :filters';
        }
      }
    }
    return $queries;
  }

  /**
   * Get the list of Drupal fields that should be used.
   *
   * @param array $chado_config
   *   A Chado config array.
   * @param bool $clear_cache
   *   TRUE to recompute values.
   *
   * @return array
   *   An array with the following structure:
   *   @code
   *   [
   *     // @todo To fill.
   *   ];
   *   @endcode
   */
  public function getRequestedChadoFields(array $chado_config, bool $clear_cache = FALSE) :array {
    if (empty($this->chadoFields) || $clear_cache) {
      // Init managed fields.
      $this->chadoFields = [];

      // Generate SQL queries with placeholders from Chado config...
      // Get Chado data mode.
      if ('table' == $chado_config['datatype']['mode']) {
        $table = $chado_config['datatype']['chado_table'];
        $id_field = "t.${table}_id";
        $this->chadoFields['id'] = [
          'mapping' => [
            'id' => 'generic',
            'config' => [
              'property_mappings' => [
                'value' => [
                  'id' => 'direct',
                  'config' => [
                    'mapping' => "${table}_id",
                  ],
                ],
              ],
            ],
            'required' => 'required',
          ],
        ];
      }
      elseif ('type_id' == $chado_config['datatype']['mode']) {
        $chado_type = $chado_config['datatype']['chado_type'];
        if (preg_match('/(.+)_(\d+)/', $chado_type, $match)) {
          $table = $match[1];
          $id_field = "t.${table}_id";
          $this->chadoFields['id'] = [
            'mapping' => [
              'id' => 'generic',
              'config' => [
                'property_mappings' => [
                  'value' => [
                    'id' => 'direct',
                    'config' => [
                      'mapping' => "${table}_id",
                    ],
                  ],
                ],
              ],
              'required' => 'required',
            ],
          ];
        }
      }
      if (!empty($table)) {
        // Get table definition.
        $table_def = $this->getChadoTableDef($table);
        if (!empty($table_def)) {
          // Default field mappings.
          $this->chadoFields['uuid'] = [
            'mapping' => [
              'id' => 'generic',
              'config' => [
                'property_mappings' => [
                  'value' => [
                    'id' => 'direct',
                    'config' => [
                      'mapping' =>
                      array_key_exists('uniquename', $table_def['fields'])
                        ? 'uniquename'
                        : "${table}_id",
                    ],
                  ],
                ],
              ],
              'required' => 'required',
            ],
          ];
          $this->chadoFields['title'] = [
            'mapping' => [
              'id' => 'generic',
              'config' => [
                'property_mappings' => [
                  'value' => [
                    'id' => 'direct',
                    'config' => [
                      'mapping' =>
                      array_key_exists('name', $table_def['fields'])
                        ? 'name'
                        : "${table}_id",
                    ],
                  ],
                ],
              ],
              'required' => 'required',
            ],
          ];

          // Get field filters.
          $fields = $chado_config['fields'];
          foreach ($fields as $field => $field_settings) {
            // Remove field prefix.
            $field_name = preg_replace('/^' . ExternalEntityTypeForm::MANAGED_FIELD_PREFIX . '/', '', $field);
            switch ($field_settings['filter']['filter_type'] ?? '') {
              case 'string':
              case 'text':
                // Manage display.
                if (!empty($field_settings['include'])) {
                  $this->chadoFields[$field] = [
                    'mapping' => [
                      'id' => 'generic',
                      'config' => [
                        'property_mappings' => [
                          'value' => [
                            'id' => 'direct',
                            'config' => [
                              'mapping' => $field_name,
                            ],
                          ],
                        ],
                      ],
                      'required' => 'required',
                    ],
                    'type' => $field_settings['filter']['filter_type'],
                    'config' => [
                      'field_name' => $field,
                      'label' => ucfirst($field_name),
                      'description' => '',
                      'required' => FALSE,
                      'settings' => [],
                    ],
                    'required' => 'required',
                  ];
                  if ('string' == $field_settings['filter']['filter_type']) {
                    $this->chadoFields[$field]['config']['settings'] = [
                      'max_length' => 255,
                    ];
                  }
                }
                break;

              case 'boolean':
                // Manage display.
                if (!empty($field_settings['include'])) {
                  $this->chadoFields[$field] = [
                    'mapping' => [
                      'id' => 'generic',
                      'config' => [
                        'property_mappings' => [
                          'value' => [
                            'id' => 'direct',
                            'config' => [
                              'mapping' => $field_name,
                            ],
                          ],
                        ],
                      ],
                      'required' => 'required',
                    ],
                    'type' => $field_settings['filter']['filter_type'],
                    'config' => [
                      'field_name' => $field,
                      'label' => ucfirst($field_name),
                      'description' => '',
                      'required' => FALSE,
                      'settings' => [
                        'on_label' => 'True',
                        'off_label' => 'False',
                      ],
                    ],
                    'required' => 'required',
                  ];
                }

                break;

              case 'decimal':
              case 'float':
              case 'integer':
                // Manage display.
                if (!empty($field_settings['include'])) {
                  $this->chadoFields[$field] = [
                    'mapping' => [
                      'id' => 'generic',
                      'config' => [
                        'property_mappings' => [
                          'value' => [
                            'id' => 'direct',
                            'config' => [
                              'mapping' => $field_name,
                            ],
                          ],
                        ],
                      ],
                      'required' => 'required',
                    ],
                    'type' => $field_settings['filter']['filter_type'],
                    'config' => [
                      'field_name' => $field,
                      'label' => ucfirst($field_name),
                      'description' => '',
                      'required' => FALSE,
                      'settings' => [
                        'min' => NULL,
                        'max' => NULL,
                        'prefix' => '',
                        'suffix' => '',
                      ],
                    ],
                    'required' => 'required',
                  ];
                }
                break;

              case 'datetime':
                // Manage display.
                if (!empty($field_settings['include'])) {
                  $this->chadoFields[$field] = [
                    'mapping' => [
                      'id' => 'generic',
                      'config' => [
                        'property_mappings' => [
                          'value' => [
                            'id' => 'direct',
                            'config' => [
                              'mapping' => $field_name,
                            ],
                          ],
                        ],
                      ],
                      'required' => 'required',
                    ],
                    'type' => $field_settings['filter']['filter_type'],
                    'config' => [
                      'field_name' => $field,
                      'label' => ucfirst($field_name),
                      'description' => '',
                      'required' => FALSE,
                      'settings' => [],
                    ],
                    'required' => 'required',
                  ];
                }
                break;

              case 'cvterm':
                // Manage display.
                // @todo Implement.
                break;

              case 'dbxref':
                // Manage display.
                // @todo Implement.
                break;

              case 'organism':
                // Manage display.
                // @todo Implement.
                break;

              default:
                $this->logger->warning(
                  'Invalid table field type '
                  . $field_settings['filter']['filter_type']
                );
                break;
            }
          }

          // Get joins.
          $joins = $chado_config['joins']['list'];
          foreach ($joins as $join_index => $join) {
            $join_name = 'j' . $join_index;
            $join_alias = strtolower(
              preg_replace('/\W/', '_', $join['join_name'])
            );

            // Note: we use e'\\x5b\\x5d' instead of '[]' to prevent Drupal
            // replacement in SQL queries.
            switch ($join['join_type'] ?? '') {
              case 'prop':
                // Manage display.
                // @todo Implement.
                break;

              case 'cvterm':
                // Manage display.
                // @todo Implement.
                break;

              case 'dbxref':
                // Manage display.
                // @todo Implement.
                break;

              case 'pub':
                // Manage display.
                // @todo Implement.
                break;

              case 'relationship':
                // Manage display.
                // @todo Implement.
                break;

              default:
                break;
            }
          }
        }
      }
    }
    return $this->chadoFields;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestedDrupalFields() :array {
    $chado_config = $this->getConfiguration()['chado_config'] ?? [];
    $chado_fields = $this->getRequestedChadoFields($chado_config);
    return array_filter(
      array_map(
        function ($request) {
          unset($request['mapping']);
          return $request;
        },
        $chado_fields ?? []
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestedMapping(string $field_name, string $field_type) :array {
    $chado_config = $this->getConfiguration()['chado_config'] ?? [];
    $chado_fields = $this->getRequestedChadoFields($chado_config);
    return $chado_fields[$field_name]['mapping'] ?? [];
  }

  /**
   * Remove all Chado settings from current config except table selection.
   */
  protected function clearChadoConfiguration(
    FormStateInterface $form_state
  ) {
    // Get current Chado config.
    $chado_config = $form_state->getValue('chado_config');
    // Cleanup values.
    $chado_config['fields'] = [];
    $chado_config['joins'] = ['list' => []];
    $form_state->setValue('chado_config', $chado_config);
    $form_state->set('join_count', 1);
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * Add a placeholder to the settings and return its name.
   *
   * @param string $value
   *   Value of the placeholder.
   *
   * @return string
   *   The placeholder name.
   */
  protected function getChadoPlaceholder($value) {
    $placeholder = ':ph_' . md5($value);
    if (!isset($this->chadoFilterValuePlaceholders)) {
      $this->chadoFilterValuePlaceholders = [];
    }
    if (!isset($this->chadoFilterValuePlaceholders[$placeholder])) {
      $this->chadoFilterValuePlaceholders[$placeholder] = $value;
    }
    return $placeholder;
  }

  /**
   * Returns available Chado instances.
   *
   * @return array
   *   Keys are database key + '#' + Chado schema name and values are Chado
   *   schema names followed by the database key in parenthesis.
   */
  public function getChadoInstances() :array {
    $all_chado_instances = [];
    $chado_dbkeys = [];
    $database_info = CoreDatabase::getAllConnectionInfo();
    $dbtool = \Drupal::service('dbxschema.tool');
    foreach ($database_info as $dbkey => $targets) {
      if (array_key_exists('default', $targets)) {
        // Check if we got a Chado database.
        if ('pgsql' == $targets['default']['driver']) {
          $connection = $dbtool->getConnection('', $dbkey);
          $chado_instances = chadol_get_available_instances($connection);
          foreach ($chado_instances as $chado_instance) {
            $chado_key = $dbkey . '#' . $chado_instance['schema_name'];
            $chado_label =
              $chado_instance['schema_name']
              . ' ('
              . $dbkey
              . ')';
            $all_chado_instances[$chado_key] = $chado_label;
          }
        }
      }
    }
    return $all_chado_instances;
  }

  /**
   * Returns a chado table definition in Drupal array format.
   *
   * @param string $table
   *   The Chado table name.
   *
   * @return array
   *   A Drupal array of a database table definition.
   */
  public function getChadoTableDef(string $table) :array {
    $table_defs = &drupal_static(__FUNCTION__);
    if (!isset($table_defs)) {
      $table_defs = [];
    }
    if (!isset($examples[$table])) {
      if (empty($this->xConnection)) {
        return [];
      }
      $table_defs[$table] = $this->xConnection->schema()->getTableDef(
        $table,
        [
          'source' => 'database',
          'format' => 'drupal',
        ]
      );
    }

    return $table_defs[$table];
  }

  /**
   * Returns an array of Chado table options.
   *
   * @return array
   *   First level keys are the table categories:
   *   - 1) Main: main tables that have associated prop, _cvterm, _dbxref or
   *        _pub tables.
   *   - 2) Secondary: associated prop, _cvterm, _dbxref or _pub tables.
   *   - 3) Relationships: _relationship tables.
   *   - 4) Tertiary: link table between two main tables (other than cvterm,
   *        dbxref and pud).
   *   - 5) Other: other non-audit tables.
   *   - 6) Audit: audit tables.
   */
  public function getChadoTables() :array {
    if (empty($this->xConnection)) {
      return [];
    }
    $chado_tables = [];
    $tables = $this->xConnection->schema()->getTables();
    foreach ($tables as $table => $table_prop) {
      // Only support tables (and not views or others) at the moment.
      if ('table' == $table_prop['type']) {
        // Sort tables.
        if ('_audit' == substr($table, -6)) {
          $chado_tables['6) Audit'][$table] = $table;
        }
        elseif (preg_match('/(?:prop|_cvterm|_dbxref|_pub)$/i', $table)) {
          $chado_tables['2) Secondary'][$table] = $table;
        }
        elseif ('_relationship' == substr($table, -13)) {
          $chado_tables['3) Relationships'][$table] = $table;
        }
        else {
          $subtables = explode('_', $table);
          if ((1 < count($subtables))
              && (array_key_exists($subtables[0], $tables)
                || array_key_exists($subtables[count($subtables) - 1], $tables))
          ) {
            $chado_tables['4) Tertiary'][$table] = $table;
          }
          elseif (
            (array_key_exists($table . 'prop', $tables)
              || array_key_exists($table . '_cvterm', $tables)
              || array_key_exists($table . '_dbxref', $tables)
              || array_key_exists($table . '_pub', $tables)
            )
            || ('db' == $table)
          ) {
            $chado_tables['1) Main'][$table] = $table;
          }
          else {
            $chado_tables['5) Other'][$table] = $table;
          }
        }
      }
    }
    return $chado_tables;
  }

  /**
   * Returns a list of Chado datatype options.
   *
   * @return array
   *   An array which first level keys are table machine names in wich the data
   *   types were found, in second level key the table machine name followed by
   *   an underscore and the value of the type_id and as value the name of the
   *   type_id from the cvterm table followed by the name of its corresponding
   *   controlled vocabulary from the cv table in parenthesis.
   */
  public function getChadoDataTypes() :array {
    if (empty($this->xConnection)) {
      return [];
    }
    $tables = $this->xConnection->schema()->getTables();
    $data_types = [];
    foreach ($tables as $table => $table_prop) {
      // Only support tables (and not views or others) at the moment.
      if (('table' == $table_prop['type'])
        && !preg_match(
          '/^cvterm|(?:_dbxref|_cvterm|_pub|prop|_relationship|_audit)$/i',
          $table
        )
      ) {
        $table_def = $this->getChadoTableDef($table);
        if (array_key_exists('type_id', $table_def['fields'])) {
          $data_types[$table] = [];
          $query = $this->xConnection->query('SELECT cvt.cvterm_id, cvt.name AS "cvterm", cv.name AS "cv" FROM {1:' . $table . '} t JOIN {1:cvterm} cvt ON cvt.cvterm_id = t.type_id JOIN {1:cv} cv ON cv.cv_id = cvt.cv_id GROUP BY 1, 2, 3;');
          foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $type) {
            if ('null' == $type['cvterm']) {
              continue;
            }
            $data_types[$table][$table . '_' . $type['cvterm_id']] = $type['cvterm'] . ' (' . $type['cv'] . ')';
          }
          if (empty($data_types[$table])) {
            unset($data_types[$table]);
          }
        }
      }
    }
    return $data_types;
  }

  /**
   * Maps an operator to its expression.
   *
   * @param string $operator
   *   The operator code.
   * @param string $field
   *   The qualified table field name.
   * @param string $formatted_value
   *   The formatted value or placeholder.
   * @param ?string $formatted_value2
   *   An optional second formatted value or placeholder.
   *
   * @return string
   *   the filtering expression or 'FALSE' if invalid operator.
   */
  public function mapOperatorExpression(
    string $operator,
    string $field,
    string $formatted_value,
    ?string $formatted_value2 = NULL,
  ) :string {

    switch ($operator) {
      // Operators ready for use.
      case '=':
      case '!=':
      case '<':
      case '<=':
      case '>=':
      case '>':
        $expression = "$field $operator $formatted_value";
        break;

      case 'i=':
        // If we need to escape user input for % or _ but we can assume the
        // admin does the job properly.
        // phpcs:disable
        // @code
        // $expression = "$field ILIKE '%' || regexp_replace($formatted_value, '([%_\\\\])', '\\\1', 'g') || '%'";.
        // @endcode
        // phpcs:enable
        $expression = "$field ILIKE $formatted_value";
        break;

      case 'i!=':
        // If we need to escape user input for % or _ but we can assume the
        // admin does the job properly.
        // phpcs:disable
        // @code
        // $expression = "$field NOT ILIKE '%' || regexp_replace($formatted_value, '([%_\\\\])', '\\\1', 'g') || '%'";.
        // @endcode
        // phpcs:enable
        $expression = "$field NOT ILIKE $formatted_value";
        break;

      // Other operators need expression adjustments.
      case 'contains':
        // If we need to escape user input for % or _ but we can assume the
        // admin does the job properly.
        // phpcs:disable
        // @code
        // $expression = "$field ILIKE '%' || regexp_replace($formatted_value, '([%_\\\\])', '\\\1', 'g') || '%'";.
        // @endcode
        // phpcs:enable
        $expression = "$field ILIKE '%' || $formatted_value || '%'";
        break;

      case 'not_contains':
        // If we need to escape user input for % or _ but we can assume the
        // admin does the job properly.
        // phpcs:disable
        // @code
        // $expression = "$field NOT ILIKE '%' || regexp_replace($formatted_value, '([%_\\\\])', '\\\1', 'g') || '%'";.
        // @endcode
        // phpcs:enable
        $expression = "$field NOT ILIKE '%' || $formatted_value || '%'";
        break;

      case 'hasword':
        // The first step escapes all non-word and non-spaces characters,
        // the second step replaces following spaces by pipes, so we would match
        // any of the given words.
        $expression = "$field ~* ('\\m(?:' || regexp_replace(regexp_replace($formatted_value, '([^ \\w])', '\\\\\\1', 'g'), ' +', '|', 'g') || ')\\M')";
        break;

      case 'hasall':
        // The first step escapes all non-word and non-spaces characters,
        // the second step replaces following spaces by lookahead, so we would
        // match any of the given words.
        $expression = "$field ~* ('(?=.*\\m' || regexp_replace(regexp_replace($formatted_value, '([^ \\w])', '\\\\\\1', 'g'), ' +', '\\M)(?=.*\\m', 'g') || '\\M)')";
        break;

      case 'begins':
        $expression = "starts_with($field, $formatted_value)";
        break;

      case 'not_begins':
        $expression = "NOT starts_with($field, $formatted_value)";
        break;

      case 'ends':
        $expression = "starts_with(reverse($field), reverse($formatted_value))";
        break;

      case 'not_ends':
        $expression = "NOT starts_with(reverse($field), reverse($formatted_value))";
        break;

      case 'shorterthan':
        $expression = "char_length($field) < $formatted_value";
        break;

      case 'longerthan':
        $expression = "char_length($field) > $formatted_value";
        break;

      case 'regex':
        $expression = "$field ~* $formatted_value";
        break;

      case '><':
        $expression = "$field BETWEEN $formatted_value AND $formatted_value2";
        break;

      case '!><':
        $expression = "$field NOT BETWEEN $formatted_value AND $formatted_value2";
        break;

      case '~*':
        $expression = "$field::text ~* $formatted_value";
        break;

      default:
        // Invalid settings: make query returns nothing.
        $this->logger->warning('Invalid filtering operator: "' . $operator . '" for field ' . $field . ' of content type ' . $this->getLabel());
        $expression = 'FALSE';
        break;
    }
    return $expression;
  }

  /**
   * Returns the Drupal field type corresponding to the given PostgreSQL type.
   *
   * @param string $type
   *   A PostgreSQL data type name.
   *
   * @return string
   *   A Drupal field type name.
   *
   * @see https://www.postgresql.org/docs/current/datatype-numeric.html
   * @see https://www.drupal.org/docs/drupal-apis/entity-api/fieldtypes-fieldwidgets-and-fieldformatters
   */
  public function mapPgsqlTypeToFieldType(string $type) :string {
    $field_type = '';
    if (in_array($type, ['integer', 'smallint', 'bigint', 'serial', 'bigserial'])) {
      $field_type = 'integer';
    }
    elseif ('numeric' == $type) {
      $field_type = 'decimal';
    }
    elseif (in_array($type, ['real', 'double precision'])) {
      $field_type = 'float';
    }
    elseif (('name' == $type)
      || ('character' == substr($type, 0, 9))
      || ('bit' == substr($type, 0, 3))
    ) {
      $field_type = 'string';
    }
    elseif (in_array($type, ['text', 'bytea'])) {
      $field_type = 'text';
    }
    elseif ('boolean' == $type) {
      $field_type = 'boolean';
    }
    elseif (in_array($type, ['date', 'time', 'timestamp'])
      || ('time ' == substr($type, 0, 5))
      || ('timestamp ' == substr($type, 0, 10))
    ) {
      $field_type = 'datetime';
    }
    elseif (!empty($type)) {
      // Unsupported formats:
      // interval, box, cidr, circle, inet, line, lseg, macaddr, money, path,
      // point, polygon.
      $this->logger->warning('Unsupported PostgreSQL type: "' . $type . '"');
      $field_type = 'string';
    }
    return $field_type;
  }

  /**
   * Returns options for field form display.
   *
   * @param string $type
   *   Field type.
   *
   * @return array
   *   Field options.
   */
  public function getFieldTypeFormDisplayOptions(string $type) :array {

    switch ($type) {
      case 'boolean':
        $widget_type = 'boolean_checkbox';
        $settings = [
          'display_label' => TRUE,
        ];
        break;

      case 'datetime':
        $widget_type = 'datetime_default';
        $settings = [];
        break;

      case 'decimal':
      case 'float':
      case 'integer':
        $widget_type = 'number';
        $settings = [
          'placeholder' => '',
        ];
        break;

      case 'string':
        $widget_type = 'string_textfield';
        $settings = [
          'size' => 60,
          'placeholder' => '',
        ];
        break;

      case 'text':
        $widget_type = 'text_textfield';
        $settings = [
          'size' => 60,
          'placeholder' => '',
        ];
        break;

      case 'cvterm':
        // @todo Implement.
        $widget_type = 'string_textfield';
        $settings = [];
        break;

      case 'dbxref':
        // @todo Implement.
        $widget_type = 'string_textfield';
        $settings = [];
        break;

      case 'organism':
        // @todo Implement.
        $widget_type = 'string_textfield';
        $settings = [];
        break;

      default:
        $widget_type = 'string_textfield';
        $settings = [];
        break;
    }

    return [
      'type' => $widget_type,
      'settings' => $settings,
    ];
  }

  /**
   * Returns options for field view display.
   *
   * @param string $type
   *   Field type.
   *
   * @return array
   *   Field options.
   */
  public function getFieldTypeViewDisplayOptions(string $type) :array {

    switch ($type) {
      case 'boolean':
        $widget_type = 'boolean';
        $settings = [
          'format' => 'default',
          'format_custom_false' => 'False',
          'format_custom_true' => 'True',
        ];
        break;

      case 'datetime':
        $widget_type = 'datetime_default';
        $settings = [
          'timezone_override' => '',
          'format_type' => 'medium',
        ];
        break;

      case 'decimal':
      case 'float':
        $widget_type = 'number_decimal';
        $settings = [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'scale' => 2,
          'prefix_suffix' => TRUE,
        ];
        break;

      case 'integer':
        $widget_type = 'number_integer';
        $settings = [
          'thousand_separator' => '',
          'prefix_suffix' => TRUE,
        ];
        break;

      case 'string':
        $widget_type = 'string';
        $settings = [
          'link_to_entity' => FALSE,
        ];
        break;

      case 'text':
        $widget_type = 'text_default';
        $settings = [];
        break;

      case 'cvterm':
        // @todo Implement.
        $widget_type = 'string';
        $settings = [];
        break;

      case 'dbxref':
        // @todo Implement.
        $widget_type = 'string';
        $settings = [];
        break;

      case 'organism':
        // @todo Implement.
        $widget_type = 'string';
        $settings = [];
        break;

      default:
        $widget_type = 'string';
        $settings = [];
        break;
    }

    return [
      'type' => $widget_type,
      'settings' => $settings,
    ];
  }

  /**
   * Set field default values.
   *
   * @param array &$form
   *   A field form element that may contain other fields.
   * @param array|bool|int|float|string $values
   *   An structured array of field values or a single value corresponding to
   *   the form structure.
   */
  public function setFieldDefaults(
    array &$form,
    array|bool|int|float|string $values
  ) {
    if (isset($values)) {
      if (is_array($values) && empty($form['#multiple'])) {
        foreach ($values as $key => $value) {
          if (array_key_exists($key, $form)) {
            $this->setFieldDefaults($form[$key], $value);
          }
        }
      }
      else {
        if (is_array($values)) {
          $form['#default_value'] = array_values($values);
        }
        else {
          $form['#default_value'] = $values;
        }
      }
    }
  }

  /**
   * Returns a form with table field settings.
   *
   * @param string $table
   *   The Chado table name.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $params
   *   An array of additional parameters with the following optional keys:
   *   - 'type_id': the Chado type_id value to filter for the given table.
   *   - 'values': an array of structured default field values keyed by field
   *     names.
   *
   * @return array
   *   A Drupal form array with table field selection and filtering.
   */
  public function getChadoTableFieldForm(
    string $table,
    FormStateInterface $form_state,
    array $params = []
  ) :array {
    $table_def = $this->getChadoTableDef($table);
    // Detect foreign key fields.
    $foreign_keys = [];
    foreach ($table_def['foreign keys'] ?? [] as $fk) {
      if (1 == count($fk['columns'])) {
        $foreign_keys[array_key_first($fk['columns'])] =
          [$fk['table'], array_values($fk['columns'])[0]];
      }
    }

    $form = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fields'),
    ];
    foreach ($table_def['fields'] as $field => $field_def) {
      $fname = ExternalEntityTypeForm::MANAGED_FIELD_PREFIX . $field;
      $form[$fname] = [
        '#type' => 'details',
        '#title' => $this->t('"@field" field', ['@field' => $field]),
        '#open' => FALSE,
      ];
      switch ($field) {
        case $table . '_id':
          // Exclude table fields already in the query.
          unset($form[$fname]);
          break;

        case 'name':
          $form[$fname]['include'] = [
            '#type' => 'hidden',
            '#value' => TRUE,
          ];
          $form[$fname]['#title'] = $this->t('Name');
          $form[$fname]['#weight'] = -25;
          $form[$fname]['filter'] = $this->getTextFilterForm();
          $form[$fname]['filter']['#type'] = 'fieldset';
          $form[$fname]['filter']['#title'] = $this->t('Filter');
          break;

        case 'uniquename':
          $form[$fname]['include'] = [
            '#type' => 'hidden',
            '#value' => TRUE,
          ];
          $form[$fname]['#title'] = $this->t('Unique name');
          $form[$fname]['#weight'] = -20;
          $form[$fname]['filter'] = $this->getTextFilterForm();
          $form[$fname]['filter']['#type'] = 'fieldset';
          $form[$fname]['filter']['#title'] = $this->t('Filter');
          break;

        case 'type_id':
          $form[$fname]['include'] = [
            '#type' => 'radios',
            '#title' => $this->t('Display:'),
            '#options' => [
              '' => $this->t('None'),
              'name' => $this->t('Type name as text'),
              // @todo Check if the builtin cvterm type is available.
              'cvtermref' => $this->t('Type name as a link to the corresponding CV term'),
            ],
            '#default_value' => '',
          ];
          $form[$fname]['#title'] = $this->t('Type');
          $form[$fname]['#weight'] = -15;
          // @todo Check if not already set by a data type selection.
          if (empty($params['type_id'])) {
            $form[$fname]['filter'] = $this->getChadoCvtermFilterForm(['table-type_id' => $table]);
            $form[$fname]['filter']['#type'] = 'fieldset';
            $form[$fname]['filter']['#title'] = $this->t('Filter');
          }
          else {
            $form[$fname]['info'] = [
              '#type' => 'markup',
              '#markup' => $this->t(
                'Type fixed by the data type selection: @type_id',
                ['@type_id' => $params['type_id']]
              ),
            ];
          }
          break;

        case 'organism_id':
          $form[$fname]['#title'] = $this->t('Organism');
          $form[$fname]['#weight'] = -10;
          $form[$fname]['include'] = [
            '#type' => 'radios',
            '#title' => $this->t('Display:'),
            '#options' => [
              '' => $this->t('None'),
              'full' => $this->t('As genus + species name + (common name)'),
              'abbreviation' => $this->t('As abbreviation'),
              'lineage' => $this->t('As lineage'),
              // @todo Check if the organism builtin type is available.
              'ref' => $this->t('As content reference'),
              'lineageref' => $this->t('As lineage references'),
            ],
            '#default_value' => '',
          ];
          $form[$fname]['filter'] = $this->getChadoOrganismFilterForm();
          $form[$fname]['filter']['#type'] = 'fieldset';
          $form[$fname]['filter']['#title'] = $this->t('Filter');
          break;

        case 'dbxref_id':
          $form[$fname]['#title'] = $this->t('Main cross-reference (dbxref)');
          $form[$fname]['#weight'] = -5;
          $form[$fname]['include'] = [
            '#type' => 'radios',
            '#title' => $this->t('Display:'),
            '#options' => [
              '' => $this->t('None'),
              'accession' => $this->t('As its accession'),
              'link' => $this->t('As a link to its source'),
              'image' => $this->t('As an image'),
              // @todo Check if the dbxref builtin type is available.
              'ref' => $this->t('As a link to the cross-reference details'),
            ],
            '#default_value' => '',
          ];
          $form[$fname]['filter'] = $this->getChadoDbxrefFilterForm();
          $form[$fname]['filter']['#type'] = 'fieldset';
          $form[$fname]['filter']['#title'] = $this->t('Filter');
          break;

        default:
          // Other fields.
          if (!empty($foreign_keys[$field])) {
            // Foreign key.
            $form[$fname]['include'] = [
              '#type' => 'radios',
              '#title' => $this->t('Display:'),
              '#options' => [
                '' => $this->t('None'),
                'accession' => $this->t('As its target name'),
                // @todo Check if the type is available.
                'ref' => $this->t('As a link to its target'),
                'render' => $this->t('As a rendered entity'),
              ],
              '#default_value' => '',
            ];
          }
          else {
            // Other type of field.
            $form[$fname]['include'] = [
              '#type' => 'checkbox',
              '#title' => $this->t(
                'Include @field',
                ['@field' => $field]
              ),
              '#default_value' => FALSE,
            ];
            $field_type = $this->mapPgsqlTypeToFieldType($field_def['pgsql_type']);
            $form[$fname]['filter'] = $this->getTypeFilterForm($field_type);
            $form[$fname]['filter']['#type'] = 'fieldset';
            $form[$fname]['filter']['#title'] = $this->t('Filter');
          }
          break;
      }
      // Set field defaults (recursively in the subform).
      if (isset($params['values'][$fname]) && isset($form[$fname])) {
        $this->setFieldDefaults(
          $form[$fname],
          $params['values'][$fname]
        );
      }
    }

    return $form;
  }

  /**
   * Returns data join form.
   *
   * @param string $table
   *   The main Chado table name.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $parent_identifier
   *   The parent form element identifier.
   *
   * @return array
   *   A Drupal form array with joinable data selection and filtering.
   */
  public function getChadoDataJoinForm(
    string $table,
    FormStateInterface $form_state,
    string $parent_identifier
  ) :array {

    $form = [
      '#type' => 'fieldset',
      '#title' => $this->t('Data Join'),
    ];

    $tables = $this->xConnection->schema()->getTables();
    $options = [];
    if (array_key_exists($table . 'prop', $tables)) {
      $options['prop'] = $this->t('Properties');
    }
    if (array_key_exists($table . '_cvterm', $tables)) {
      $options['cvterm'] = $this->t('Controlled Vocabularies');
    }
    if (array_key_exists($table . '_dbxref', $tables)) {
      $options['dbxref'] = $this->t('Cross-references');
    }
    if (array_key_exists($table . '_pub', $tables)) {
      $options['pub'] = $this->t('Publications');
    }
    if (array_key_exists($table . '_relationship', $tables)) {
      $options['relationship'] = $this->t('Relationships');
    }

    // @todo Detect tables with foreign key pointing to this table id field.
    // ex.: featureloc
    $fk_query = "
      SELECT
        r.table_name, r.column_name
      FROM information_schema.constraint_column_usage u
        JOIN information_schema.referential_constraints fk
          ON u.constraint_catalog = fk.unique_constraint_catalog
            AND u.constraint_schema = fk.unique_constraint_schema
            AND u.constraint_name = fk.unique_constraint_name
        JOIN information_schema.key_column_usage r
          ON r.constraint_catalog = fk.constraint_catalog
            AND r.constraint_schema = fk.constraint_schema
            AND r.constraint_name = fk.constraint_name
      WHERE
        u.column_name = :column
        AND u.table_name = :table
        AND u.table_schema = :schema
        AND u.table_catalog = :db
        AND r.table_name !~* '(?:_audit|_cvterm|_dbxref|_pub|_relationship|prop)\$'
    ";
    $fk_tables = [];
    try {
      $fk_table_results = $this
        ->xConnection
        ->query(
          $fk_query,
          [
            ':column' => $table . '_id',
            ':table' => $table,
            ':schema' => $this->xConnection->getSchemaName(),
            ':db' => $this->xConnection->getDatabaseName(),
          ]
        )
        ->fetchAll();
      foreach ($fk_table_results as $fk_table) {
        // Check if it is a link table.
        $other_table = preg_replace('/^' . $table . '_|_' . $table . '$/', '', $fk_table->table_name);
        if (($other_table != $fk_table->table_name)
          && array_key_exists($other_table, $tables)
        ) {
          $fk_tables[] = $fk_table->table_name . '__' . $fk_table->column_name . '__' . $other_table;
        }
        else {
          $fk_tables[] = $fk_table->table_name . '__' . $fk_table->column_name;
        }
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      // Ignore error.
    }
    foreach ($fk_tables as $fk_table_code) {
      $fk_info = explode('__', $fk_table_code);
      // Check for many-to-many or 1-to-many relationship.
      if (2 < count($fk_info)) {
        $fk_table_name = $fk_info[2];
      }
      else {
        $fk_table_name = $fk_info[0];
      }
      // Check if the foreign key must be displayed.
      if ($fk_info[1] != $table . '_id') {
        $options['fk__' . $fk_table_code] = $fk_table_name . ' (as ' . $fk_info[1] . ')';
      }
      else {
        $options['fk__' . $fk_table_code] = $fk_table_name;
      }

    }

    if (empty($options)) {
      $form['none'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No data join detected.'),
      ];
      return $form;
    }

    // Build "joins" list.
    $join_count = $form_state->get('join_count');
    if (empty($join_count)) {
      $join_count =
        max(
          count($this->configuration['chado_config']['joins']['list'] ?? []),
          1
        );
    }
    $form_state->set('join_count', $join_count);

    for ($i = 0; $i < $join_count; ++$i) {
      $join_id = uniqid('joindata', TRUE);
      $join_label = (
        $this->configuration['chado_config']['joins']['list'][$i]['join_name']
        ?? $this->t('+ New join')
      );
      $form['list'][$i] = [
        '#type' => 'details',
        '#title' => $join_label,
        '#open' => (1 < $join_count),
      ];

      // Add "remove" option if the join already exists.
      $data_type_options = $options;
      if (!empty($this->configuration['chado_config']['joins']['list'][$i])) {
        $data_type_options['remove'] = $this->t('- Remove this join');
        // Close existing "joins" to reduce form display length.
        $form['list'][$i]['#open'] = FALSE;
      }
      $form['list'][$i]['join_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Data type'),
        '#options' => $data_type_options,
        '#attributes' => [
          'data-chadol-selector' => $join_id,
        ],
        '#default_value' => $this->configuration['chado_config']['joins']['list'][$i]['join_type'] ?? NULL,
      ];
      $form['list'][$i]['join_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Content field name'),
        '#default_value' =>
        $this->configuration['chado_config']['joins']['list'][$i]['join_name']
        ?? NULL,
      ];

      $form['list'][$i]['required'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Require this join'),
        '#description' => $this->t(
          '(will filter out the Chado content if no data available for this join)'
        ),
        '#default_value' =>
        $this->configuration['chado_config']['joins']['list'][$i]['required']
        ?? NULL,
      ];

      if (!empty($options['prop'])) {
        $form['list'][$i]['join_prop'] = $this->getChadoTablePropFilterForm();
        $form['list'][$i]['join_prop']['#states'] = [
          'visible' => [
            'select[data-chadol-selector="' . $join_id . '"]' =>
              ['value' => 'prop'],
          ],
        ];
      }
      if (!empty($options['cvterm'])) {
        $form['list'][$i]['join_cvterm'] =
          $this->getChadoTableCvtermFilterForm();
        $form['list'][$i]['join_cvterm']['#states'] = [
          'visible' => [
            'select[data-chadol-selector="' . $join_id . '"]' =>
              ['value' => 'cvterm'],
          ],
        ];
      }
      if (!empty($options['dbxref'])) {
        $form['list'][$i]['join_dbxref'] =
          $this->getChadoTableDbxrefFilterForm();
        $form['list'][$i]['join_dbxref']['#states'] = [
          'visible' => [
            'select[data-chadol-selector="' . $join_id . '"]' =>
              ['value' => 'dbxref'],
          ],
        ];
      }
      if (!empty($options['pub'])) {
        $form['list'][$i]['join_pub'] =
          $this->getChadoTablePubFilterForm();
        $form['list'][$i]['join_pub']['#states'] = [
          'visible' => [
            'select[data-chadol-selector="' . $join_id . '"]' =>
              ['value' => 'pub'],
          ],
        ];
      }
      if (!empty($options['relationship'])) {
        $form['list'][$i]['join_rel'] =
          $this->getChadoTableRelationshipFilterForm();
        $form['list'][$i]['join_rel']['#states'] = [
          'visible' => [
            'select[data-chadol-selector="' . $join_id . '"]' =>
              ['value' => 'relationship'],
          ],
        ];
      }
      if (!empty($fk_tables)) {
        foreach ($fk_tables as $fk_table) {
          $fk_info = explode('__', $fk_table);
          $form['list'][$i]['join_' . $fk_table] = [
            '#type' => 'container',
            'include' => [
              '#type' => 'checkbox',
              '#title' => $this->t(
                'Include @table',
                [
                  '@table' => (2 < count($fk_info)
                    ? $fk_info[2]
                    : $fk_info[0] . '.' . $fk_info[1]
                  ),
                ]
              ),
              // @todo Add links to tabs.
              '#description' => $this->t(
                'If included, you can manage the referenced entities using the corresponding field in "@manage_fields" and in "@manage_display".',
                [
                  '@manage_fields' => $this->t('Manage fields'),
                  '@manage_display' => $this->t('Manage display'),
                ]
              ),
            ],
          ];
          $form['list'][$i]['join_' . $fk_table]['#states'] = [
            'visible' => [
              'select[data-chadol-selector="' . $join_id . '"]' =>
                ['value' => 'fk__' . $fk_table],
            ],
          ];
        }
      }
      $this->setFieldDefaults(
        $form['list'][$i],
        $this->configuration['chado_config']['joins']['list'][$i] ?? []
      );
    }

    $form['add_join'] = [
      '#type' => 'submit',
      '#value' => 'Add new join',
      '#name' => 'add_join',
      '#ajax' => [
        'callback' => [
          get_class($this),
          'buildAjaxParentSubForm',
        ],
        'wrapper' => $parent_identifier,
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    return $form;
  }

  /**
   * Returns a filter form corresponding to the given type.
   *
   * @param string $type
   *   A Drupal field type name.
   *
   * @return array
   *   A Drupal form array with filtering options.
   */
  public function getTypeFilterForm(string $type) :array {
    $form = [];
    switch ($type) {
      case 'integer':
      case 'decimal':
      case 'float':
        $form = $this->getNumericFilterForm();
        break;

      case 'string':
      case 'text':
        $form = $this->getTextFilterForm();
        break;

      case 'boolean':
        $form = $this->getBooleanFilterForm();
        break;

      case 'datetime':
        $form = $this->getDateFilterForm();
        break;

      default:
        // Unsupported format.
        $this->logger->warning(
          'Unsupported field type for filtering: "' . $type . '"'
        );
        break;
    }
    return $form;
  }

  /**
   * Returns a text filter form.
   *
   * @param array $params
   *   An associative array of parameters:
   *   - 'type': a Drupal field type. Sets the associated field type.
   *
   * @return array
   *   A Drupal form array with filtering options.
   */
  public function getTextFilterForm(
    array $params = []
  ) :array {
    $form = [
      '#type' => 'container',
    ];

    $form['filter_type'] = [
      '#type' => 'hidden',
      '#value' => $params['type'] ?? 'text',
    ];

    $form['filter_op'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#empty_value' => '',
      '#options' => [
        'i='            => $this->t('Is equal to'),
        'i!='           => $this->t('Is not equal to'),
        'contains'     => $this->t('Contains'),
        'hasword'      => $this->t('Contains any word'),
        'hasall'       => $this->t('Contains all words'),
        'begins'       => $this->t('Starts with'),
        'not_begins'   => $this->t('Does not start with'),
        'ends'         => $this->t('Ends with'),
        'not_ends'     => $this->t('Does not end with'),
        'not_contains' => $this->t('Does not contain'),
        'shorterthan'  => $this->t('Length is shorter than'),
        'longerthan'   => $this->t('Length is longer than'),
        'regex'        => $this->t('Regular expression'),
      ],
    ];

    $form['filter_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
    ];

    return $form;
  }

  /**
   * Returns SQL conditions to filter a text.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   The qualified table field name.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getTextFilterConditions(
    array $settings,
    string $table_field
  ) :array {

    $conditions = [];

    if (!empty($settings['filter_op'])) {
      $placeholder = $this->getChadoPlaceholder($settings['filter_value']);
      $conditions[] = $this->mapOperatorExpression(
        $settings['filter_op'],
        $table_field,
        $placeholder
      );
    }

    return $conditions;
  }

  /**
   * Returns a boolean filter form.
   *
   * @param array $params
   *   An associative array of parameters:
   *   - 'type': a Drupal field type. Sets the associated field type.
   *
   * @return array
   *   A Drupal form array with filtering options.
   */
  public function getBooleanFilterForm(
    array $params = []
  ) :array {
    $form = [
      '#type' => 'container',
    ];

    $form['filter_type'] = [
      '#type' => 'hidden',
      '#value' => $params['type'] ?? 'boolean',
    ];

    $form['filter_value'] = [
      '#type' => 'select',
      '#title' => $this->t('Value is'),
      '#empty_value' => '',
      '#options' => [
        'true'  => $this->t('True'),
        'false' => $this->t('False'),
      ],
    ];

    return $form;
  }

  /**
   * Returns SQL conditions to filter a boolean.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   The qualified table field name.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getBooleanFilterConditions(
    array $settings,
    string $table_field
  ) :array {
    $conditions = [];
    switch ($settings['filter_value'] ?? 'missing') {
      case 'true':
        $conditions[] = "$table_field IS TRUE";
        break;

      case 'false':
        $conditions[] = "$table_field IS FALSE";
        break;

      case '':
        break;

      default:
        $this->logger->warning(
          'Invalid boolean value for condition: "'
          . $settings['filter_value']
          . '"'
        );
        break;
    }
    return $conditions;
  }

  /**
   * Returns a numeric filter form.
   *
   * @param array $params
   *   An associative array of parameters:
   *   - 'type': a Drupal field type. Sets the associated field type.
   *
   * @return array
   *   A Drupal form array with filtering options.
   */
  public function getNumericFilterForm(
    array $params = []
  ) :array {
    $field_id = uniqid('field', TRUE);

    $form = [
      '#type' => 'container',
    ];

    $form['filter_type'] = [
      '#type' => 'hidden',
      '#value' => $params['type'] ?? 'numeric',
    ];

    $form['filter_op'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#empty_value' => '',
      '#options' => [
        '<'   => $this->t('Is less than'),
        '<='  => $this->t('Is less than or equal to'),
        '='   => $this->t('Is equal to'),
        '!='  => $this->t('Is not equal to'),
        '>='  => $this->t('Is greater than or equal to'),
        '>'   => $this->t('Is greater than'),
        '><'  => $this->t('Is between'),
        '!><' => $this->t('Is not between'),
        '~*'  => $this->t('Regular expression'),
      ],
      '#attributes' => [
        'data-chadol-selector' => $field_id,
      ],
    ];

    $form['filter_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#states' => [
        'invisible' => [
          'select[data-chadol-selector="' . $field_id . '"]' => [
            ['value' => '><'],
            'or',
            ['value' => '!><'],
          ],
        ],
      ],
    ];

    $form['filter_min'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Min'),
      '#states' => [
        'visible' => [
          'select[data-chadol-selector="' . $field_id . '"]' => [
            ['value' => '><'],
            'or',
            ['value' => '!><'],
          ],
        ],
      ],
    ];

    $form['filter_max'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max'),
      '#states' => [
        'visible' => [
          'select[data-chadol-selector="' . $field_id . '"]' => [
            ['value' => '><'],
            'or',
            ['value' => '!><'],
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Returns SQL conditions to filter a numeric value.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   The qualified table field name.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getNumericFilterConditions(
    array $settings,
    string $table_field
  ) :array {

    $conditions = [];

    if (!empty($settings['filter_op'])) {
      if (in_array($settings['filter_op'], ['><', '!><'])) {
        // "between" operator case has 2 values, min and max.
        $placeholder = $this->getChadoPlaceholder($settings['filter_min']);
        $placeholder2 = $this->getChadoPlaceholder($settings['filter_max']);
        $conditions[] = $this->mapOperatorExpression(
          $settings['filter_op'],
          $table_field,
          $placeholder,
          $placeholder2
        );
      }
      else {
        // Other classic cases.
        $placeholder = $this->getChadoPlaceholder($settings['filter_value']);
        $conditions[] = $this->mapOperatorExpression(
          $settings['filter_op'],
          $table_field,
          $placeholder
        );
      }
    }

    return $conditions;
  }

  /**
   * Returns a date filter form.
   *
   * @param array $params
   *   An associative array of parameters:
   *   - 'type': a Drupal field type. Sets the associated field type.
   *
   * @return array
   *   A Drupal form array with filtering options.
   */
  public function getDateFilterForm(
    array $params = []
  ) :array {

    $params['type'] = $params['type'] ?? 'datetime';
    $form = $this->getNumericFilterForm($params);
    unset($form['filter_op']['#options']['regex']);

    $form['filter_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Value type'),
      '#options' => [
        'date' => $this->t(
          'A date in any machine readable format. CCYY-MM-DD HH:MM:SS is preferred.'
        ),
        'offset' => $this->t(
          'An offset from the current time such as "@example1" or "@example2"',
          ['@example1' => '+1 day', '@example2' => '-2 hours -30 minutes']
        ),
      ],
      '#default_value' => 'date',
    ];
    return $form;
  }

  /**
   * Returns SQL conditions to filter a date.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   The qualified table field name.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getDateFilterConditions(
    array $settings,
    string $table_field
  ) :array {

    $conditions = [];

    if (!empty($settings['filter_op'])) {
      if (in_array($settings['filter_op'], ['><', '!><'])) {
        // "between" operator case has 2 values, min and max.
        switch ($settings['filter_mode'] ?? '') {
          case 'date':
            $from_date_value = new DrupalDateTime($settings['filter_min']);
            $to_date_value = new DrupalDateTime($settings['filter_max']);
            break;

          case 'offset':
            $from_offset = \DateInterval::createFromDateString($settings['filter_min']);
            $from_date_value = new DrupalDateTime();
            $from_date_value->add($from_offset);

            $to_offset = \DateInterval::createFromDateString($settings['filter_max']);
            $to_date_value = new DrupalDateTime();
            $to_date_value->add($to_offset);
            break;

          default:
            break;
        }
        $placeholder = $this->getChadoPlaceholder($from_date_value->format(\DateTime::ATOM));
        $placeholder2 = $this->getChadoPlaceholder($to_date_value->format(\DateTime::ATOM));
        $conditions[] = $this->mapOperatorExpression($settings['filter_op'], $table_field, $placeholder, $placeholder2);
      }
      else {
        // Other classic cases.
        switch ($settings['filter_mode'] ?? '') {
          case 'date':
            $date_value = new DrupalDateTime($settings['filter_value']);
            break;

          case 'offset':
            $offset = \DateInterval::createFromDateString($settings['filter_value']);
            $date_value = new DrupalDateTime();
            $date_value->add($offset);
            break;

          default:
            break;
        }
        $placeholder = $this->getChadoPlaceholder($date_value->format(\DateTime::ATOM));
        $conditions[] = $this->mapOperatorExpression($settings['filter_op'], $table_field, $placeholder);
      }
    }

    return $conditions;
  }

  /**
   * Returns SQL conditions to filter a timestamp date.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   The qualified table field name.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getDateTimestampFilterConditions(
    array $settings,
    string $table_field
  ) :array {
    // Cast value to timestamp.
    $table_field = 'to_timestamp(CAST(' . $table_field . ' AS double precision))';
    return $this->getDateFilterConditions($settings, $table_field);
  }

  /**
   * Returns SQL conditions to filter a ISO 8601 date.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   The qualified table field name.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getDateIso8601FilterConditions(
    array $settings,
    string $table_field
  ) :array {
    return $this->getDateFilterConditions($settings, $table_field);
  }

  /**
   * Returns a CV Term filter form.
   *
   * @param array $params
   *   An associative array of parameters:
   *   - table-type_id: will list or autocomplete on the given table name
   *     .type_id field values.
   *   - table-cvterm_id: will list or autocomplete on the given table name
   *     .cvterm_id field values.
   *   - cv_id: will list or autocomplete on the given CV.
   *
   * @return array
   *   A Drupal form array with CV Term filtering options.
   */
  public function getChadoCvtermFilterForm(
    array $params = []
  ) :array {
    $max_items = 200;
    $join_id = uniqid('joincvt', TRUE);

    $form = [
      '#type' => 'container',
    ];

    $form['filter_type'] = [
      '#type' => 'hidden',
      '#value' => 'cvterm',
    ];

    $form['filter_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Only terms from...'),
      '#options' => [
        'cv' => $this->t('... a controlled vocabulary'),
        'cvterm' => $this->t('... a category (ie. "children" terms of a term)'),
        'list' => $this->t('... a custom list'),
        '' => $this->t('not filtered'),
      ],
      '#attributes' => [
        'data-chadol-selector' => $join_id,
      ],
      '#default_value' => '',
    ];

    // Build CV list.
    if (!empty($params['table-type_id'])) {
      // Limit terms to type_id of a given table.
      $query = "SELECT cv.cv_id, cv.name FROM {1:cvterm} cvt JOIN {1:"
        . $params['table-type_id']
        . "} t ON cvt.cvterm_id = t.type_id  JOIN {1:cv} cv USING (cv_id) GROUP BY 1, 2 ORDER BY 2, 1;";
    }
    elseif (!empty($params['table-cvterm_id'])) {
      // Limit terms to type_id of a given table.
      $query =
        "SELECT cv.cv_id, cv.name FROM {1:cvterm} cvt JOIN {1:"
        . $params['table-cvterm_id']
        . "} t ON cvt.cvterm_id = t.cvterm_id  JOIN {1:cv} cv USING (cv_id) GROUP BY 1, 2 ORDER BY 2, 1;";
    }
    else {
      $query = 'SELECT cv.cv_id, cv.name FROM {1:cv} cv ORDER BY 2, 1;';
    }

    $cv_results = $this->xConnection->query($query)->fetchAll();
    $cv_options = [];
    foreach ($cv_results as $cv_result) {
      $cv_options[$cv_result->cv_id] = $cv_result->name;
    }

    $form['filter_cv'] = [
      '#type' => 'select',
      '#title' => $this->t('Controlled Vocabulary'),
      '#options' => $cv_options,
      '#states' => [
        'visible' => [
          'input[data-chadol-selector="' . $join_id . '"]' => ['value' => 'cv'],
        ],
      ],
    ];

    // Category.
    $form['filter_cvterm'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category (ie. "parent" term)'),
      '#autocomplete_route_name' => 'chadol.autocomplete',
      '#autocomplete_route_parameters' => [
        'dbkey' => $this->configuration['connection']['dbkey'] ?? 'default',
        'schema' => $this->configuration['connection']['schemas'][0] ?? '',
        'type' => 'cvterm',
        'params' => '',
      ],
      '#states' => [
        'visible' => [
          'input[data-chadol-selector="' . $join_id . '"]' => ['value' => 'cvterm'],
        ],
      ],
    ];

    // List of terms.
    if (empty($params['autocomplete'])) {
      if (!empty($params['table-type_id'])) {
        // Limit terms to type_id of a given table.
        $query =
          "SELECT cv.cv_id, cv.name AS \"cv\", cvt.cvterm_id, cvt.name FROM {1:cvterm} cvt JOIN {1:"
          . $params['table-type_id']
          . "} t ON cvt.cvterm_id = t.type_id  JOIN {1:cv} cv USING (cv_id) GROUP BY 1, 2, 3, 4 ORDER BY 2, 1, 4, 3 LIMIT $max_items;";
      }
      elseif (!empty($params['table-cvterm_id'])) {
        // Limit terms to type_id of a given table.
        $query =
          "SELECT cv.cv_id, cv.name AS \"cv\", cvt.cvterm_id, cvt.name FROM {1:cvterm} cvt JOIN {1:"
          . $params['table-cvterm_id']
          . "} t ON cvt.cvterm_id = t.cvterm_id  JOIN {1:cv} cv USING (cv_id) GROUP BY 1, 2, 3, 4 ORDER BY 2, 1, 4, 3 LIMIT $max_items;";
      }
      else {
        $query = "SELECT cv.cv_id, cv.name AS \"cv\", cvt.cvterm_id, cvt.name FROM {1:cv} cv JOIN {1:cvterm} cvt USING (cv_id) ORDER BY 2, 1, 4, 3 LIMIT $max_items;";
      }

      $cvt_results = $this->xConnection->query($query)->fetchAll();
      if ((0 < count($cvt_results)) && ($max_items > count($cvt_results))) {
        // We got normal results.
        $cvterm_options = [];
        foreach ($cvt_results as $cvt_result) {
          $cvterm_options[$cvt_result->cv][$cvt_result->cvterm_id] =
            $cvt_result->name;
        }

        $form['filter_list'] = [
          '#type' => 'select',
          '#title' => $this->t('List of terms'),
          '#options' => $cvterm_options,
          '#multiple' => TRUE,
          '#states' => [
            'visible' => [
              'input[data-chadol-selector="' . $join_id . '"]' => ['value' => 'list'],
            ],
          ],
        ];
      }
    }

    if (empty($form['filter_list'])) {
      // Filter list not set either because autocomplete was requested or
      // because we got no results or more than $max_items.
      // Set autocomplete filter.
      $auto_params = '';
      if (!empty($params['table-type_id'])) {
        $auto_params = 'table-type_id-' . $params['table-type_id'];
      }
      elseif (!empty($params['table-cvterm_id'])) {
        $auto_params = 'table-cvterm_id-' . $params['table-cvterm_id'];
      }
      $form['filter_list'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Coma-separated list of terms (with "[id:cvterm_id]")'),
        '#description' => $this->t('If you wish to use cvterm_ids directly, specify them using the syntax "[id:<cvterm_id>]". Ex.: "[id:1234],[id:5678],[id:91011]".'),
        '#autocomplete_route_name' => 'chadol.autocomplete',
        '#autocomplete_route_parameters' => [
          'dbkey' => $this->configuration['connection']['dbkey'] ?? 'default',
          'schema' => $this->configuration['connection']['schemas'][0] ?? '',
          'type' => 'cvterms',
          'params' => $auto_params,
        ],
        '#states' => [
          'visible' => [
            'input[data-chadol-selector="' . $join_id . '"]' => ['value' => 'list'],
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * Returns SQL conditions to filter a CV Term.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   The qualified table field name.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getChadoCvtermFilterConditions(
    array $settings,
    string $table_field
  ) :array {

    $conditions = [];
    switch ($settings['filter_mode']) {
      case 'cv':
        $conditions[] = $table_field . ' IN (SELECT cvterm_id FROM cvterm WHERE cv_id = ' . intval($settings['filter_cv']) . ')';
        break;

      case 'cvterm':
        if (preg_match_all('/\[id:(\d+)\]/', $settings['filter_cvterm'], $matches)) {
          $conditions[] =
            $table_field
            . " IN (
              WITH RECURSIVE cat AS (
                SELECT cvt.cvterm_id, cvt.cv_id
                FROM cvterm cvt
                JOIN cvterm_relationship cvtr ON cvtr.subject_id = cvt.cvterm_id
                JOIN cvterm cvt2 ON cvt2.cvterm_id = cvtr.object_id AND cvt2.cv_id = cvt.cv_id
                JOIN cvterm cvt3 ON cvt3.cvterm_id = cvtr.type_id AND cvt3.cv_id = cvt.cv_id
              WHERE
                cvt2.cvterm_id = ("
            . implode(',', $matches[1])
            . ")
                AND cvt3.name = 'is_a'
              UNION ALL
                SELECT cvt.cvterm_id, cvt.cv_id
                FROM cvterm cvt
                  JOIN cvterm_relationship cvtr ON cvtr.subject_id = cvt.cvterm_id
                  JOIN cat cvt2 ON cvt2.cvterm_id = cvtr.object_id AND cvt2.cv_id = cvt.cv_id
                  JOIN cvterm cvt3 ON cvt3.cvterm_id = cvtr.type_id AND cvt3.cv_id = cvt.cv_id
                WHERE
                  cvt3.name = 'is_a'
              )
              SELECT DISTINCT cat.cvterm_id FROM cat)";
        }
        break;

      case 'list':
        $filter_list = $settings['filter_list'];
        if (!is_array($filter_list)) {
          // Autocomplete case.
          if (preg_match_all('/\[id:(\d+)\]/', $filter_list, $matches)) {
            $filter_list = $matches[1];
          }
          else {
            // Select case with single value.
            $filter_list = intval($filter_list);
          }
        }
        else {
          // Make sure we only got identifiers.
          $filter_list = array_filter($filter_list, 'intval');
        }
        if (!empty($filter_list)) {
          $conditions[] = $table_field . ' IN (' . implode(',', $filter_list) . ')';
        }
        break;

      default:
        break;
    }

    return $conditions;
  }

  /**
   * Returns a DBXRef filter form.
   *
   * @param array $params
   *   An associative array of parameters:
   *   - table-dbxref_id: will list only databases related to dbxref found in
   *     the given table.
   *   - single: will not allow multiple value selection if TRUE.
   *
   * @return array
   *   A Drupal form array with DBXRef filtering options.
   */
  public function getChadoDbxrefFilterForm(
    array $params = []
  ) :array {
    $form = [
      '#type' => 'container',
    ];

    $form['filter_type'] = [
      '#type' => 'hidden',
      '#value' => 'dbxref',
    ];

    if (!empty($params['table-dbxref_id'])) {
      // Limit databases to database related to dbxref_id of a given table.
      $query = "
        SELECT db.db_id, db.name
        FROM {1:db} db
          JOIN {1:dbxref} dbx USING (db_id)
          JOIN {1:" . $params['table-dbxref_id'] . "} t
            ON dbx.dbxref_id = t.dbxref_id
        GROUP BY 1, 2
        ORDER BY 2, 1;
      ";
    }
    else {
      $query = 'SELECT db.db_id, db.name FROM {1:db} db ORDER BY 2, 1;';
    }

    $db_results = $this->xConnection->query($query)->fetchAll();
    $db_options = [];
    foreach ($db_results as $db_result) {
      $db_options[$db_result->db_id] = $db_result->name;
    }

    $form['filter_db'] = [
      '#type' => 'select',
      '#title' => $this->t('Cross-reference Database'),
      '#multiple' => empty($params['single']),
      '#options' => $db_options,
    ];

    // @todo Maybe add the possibility to filter a list of DBXRef by ID or by
    //   text filtering on their accession.
    return $form;
  }

  /**
   * Returns SQL conditions to filter a DBXRef.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   The qualified table field name.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getChadoDbxrefFilterConditions(
    array $settings,
    string $table_field
  ) :array {

    $conditions = [];
    if (!empty($settings['filter_db'])) {
      // We make sure we got valid identifiers (0 values are filtered).
      $values = array_filter(
        $settings['filter_db'],
        function ($x) {
          return intval($x);
        }
      );
      if (!empty($values)) {
        $conditions[] =
          $table_field
          . ' IN (SELECT dbxref_id FROM dbxref WHERE db_id IN ('
          . implode(',', $values)
          . '))';
      }
      else {
        $this->logger->warning(
          'Invalid DB values for DBXRef filtering ('
          . $table_field
          . '): "'
          . implode(',', $join['filter_db'])
          . '"'
        );
      }
    }

    return $conditions;
  }

  /**
   * Returns an organism filter form.
   *
   * @param array $params
   *   An associative array of parameters. Currently not used.
   *
   * @return array
   *   A Drupal form array with organism filtering options.
   */
  public function getChadoOrganismFilterForm(
    array $params = []
  ) :array {
    $organism_filter_id = uniqid('joinorg', TRUE);
    $form = [
      '#type' => 'container',
    ];

    $form['filter_type'] = [
      '#type' => 'hidden',
      '#value' => 'organism',
    ];

    $form['filter_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Only'),
      '#options' => [
        'tree' => $this->t('from a given phylogenetic tree'),
        'descendent' => $this->t('descendents of a given phylogenetic tree node'),
        'text' => $this->t('if "genus name" passes the filter'),
        '' => $this->t('not filtered'),
      ],
      '#attributes' => [
        'data-chadol-selector' => $organism_filter_id,
      ],
      '#default_value' => '',
    ];

    // Phylogenetic tree.
    $query = 'SELECT tr.phylotree_id, tr.name FROM {1:phylotree} tr ORDER BY 1, 2;';
    $tree_results = $this->xConnection->query($query)->fetchAll();
    $tree_options = [];
    foreach ($tree_results as $tree_result) {
      $tree_options[$tree_result->phylotree_id] = $tree_result->name;
    }

    $form['filter_tree'] = [
      '#type' => 'select',
      '#title' => $this->t('Phylogenetic Tree'),
      '#options' => $tree_options,
      '#states' => [
        'visible' => [
          'input[data-chadol-selector="' . $organism_filter_id . '"]' => ['value' => 'tree'],
        ],
      ],
    ];

    // Node descendents.
    $form['filter_node'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parent phylogenetic node'),
      '#autocomplete_route_name' => 'chadol.autocomplete',
      '#autocomplete_route_parameters' => [
        'dbkey' => $this->configuration['connection']['dbkey'] ?? 'default',
        'schema' => $this->configuration['connection']['schemas'][0] ?? '',
        'type' => 'phylonode',
        'params' => '',
      ],
      '#states' => [
        'visible' => [
          'input[data-chadol-selector="' . $organism_filter_id . '"]' => ['value' => 'descendent'],
        ],
      ],
    ];

    // Text filter on genus, species.
    $form['filter_text'] = $this->getTextFilterForm();
    $form['filter_text']['#type'] = 'fieldset';
    $form['filter_text']['#title'] = $this->t('Filter on "genus species" name');
    $form['filter_text']['filter_value']['#title'] = $this->t('"Genus species" filter');
    $form['filter_text']['#states'] = [
      'visible' => [
        'input[data-chadol-selector="' . $organism_filter_id . '"]' => ['value' => 'text'],
      ],
    ];

    return $form;
  }

  /**
   * Returns SQL conditions to filter an organism.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $table_field
   *   Qualified name of the field holding the organism id value.
   * @param string $main_table_alias
   *   The alias of the table holding the organism_id field to use.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getChadoOrganismFilterConditions(
    array $settings,
    string $table_field,
    string $main_table_alias = 't'
  ) :array {

    $conditions = [];

    // Filter value.
    switch ($settings['filter_mode']) {
      case 'tree':
        if (!empty($settings['filter_tree'])) {
          $conditions[] =
              "EXISTS (
                SELECT TRUE
                FROM phylonode_organism po
                  JOIN phylonode p ON p.phylonode_id = po.phylonode_id
                WHERE
                  po.organism_id = ${main_table_alias}.organism_id
                  AND p.phylotree_id ="
              . intval($settings['filter_tree'])
              . ")";
        }
        break;

      case 'descendent':
        if (!empty($settings['filter_node'])) {
          if (preg_match_all('/\[id:(\d+)\]/', $settings['filter_node'], $matches)) {
            $conditions[] =
              "EXISTS (
                SELECT TRUE
                FROM phylonode_organism po
                  JOIN phylonode pd ON pd.phylonode_id = po.phylonode_id,
                  phylonode pp
                WHERE
                  po.organism_id = ${main_table_alias}.organism_id
                  AND pp.phylonode_id = "
              . $matches[1][0]
              . " AND pd.phylotree_id = pp.phylotree_id
                  AND pd.left_idx >= pp.left_idx
                  AND pd.right_idx <= pp.right_idx)";
          }
        }
        break;

      case 'text':
        $genus_species = "(SELECT genus || ' ' || species FROM organism WHERE organism_id = $table_field)";
        $conditions = array_merge(
          $conditions,
          $this->getTextFilterConditions($settings['filter_text'], $genus_species)
        );
        break;

      default:
        break;
    }

    return $conditions;
  }

  /**
   * Returns a property table filter form.
   *
   * @param array $params
   *   An associative array of parameters. See self::getChadoCvtermFilterForm().
   *
   * @return array
   *   A Drupal form array with property table filtering.
   */
  public function getChadoTablePropFilterForm(
    array $params = []
  ) :array {
    $prop_filter_id = uniqid('joinprop', TRUE);
    $form = [
      '#type' => 'fieldset',
      '#title' => $this->t('Property filtering'),
    ];

    // Filter type_id.
    $form['filter_type_id'] = $this->getChadoCvtermFilterForm($params);
    $form['filter_type_id']['#type'] = 'fieldset';
    $form['filter_type_id']['#title'] = $this->t('Filter type');
    $form['filter_type_id']['filter_mode']['#title'] = $this->t('Filter type: only terms from...');

    // Filter value.
    $form['filter_value'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter property value'),
    ];
    $form['filter_value']['filter_value_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Value type'),
      '#options' => [
        'text' => $this->t('Text'),
        'numeric' => $this->t('Numeric'),
        'timestamp' => $this->t('Timestamp date'),
        'iso8601' => $this->t('ISO 8601 date'),
      ],
      '#attributes' => [
        'data-chadol-selector' => $prop_filter_id,
      ],
      '#default_value' => 'text',
    ];

    $form['filter_value']['filter_value_text'] = $this->getTextFilterForm($params);
    $form['filter_value']['filter_value_text']['#states'] = [
      'visible' => [
        'input[data-chadol-selector="' . $prop_filter_id . '"]' => ['value' => 'text'],
      ],
    ];

    $form['filter_value']['filter_value_numeric'] = $this->getNumericFilterForm($params);
    $form['filter_value']['filter_value_numeric']['#states'] = [
      'visible' => [
        'input[data-chadol-selector="' . $prop_filter_id . '"]' => ['value' => 'numeric'],
      ],
    ];

    $form['filter_value']['filter_value_date'] = $this->getDateFilterForm($params);
    $form['filter_value']['filter_value_date']['#states'] = [
      'visible' => [
        'input[data-chadol-selector="' . $prop_filter_id . '"]' => [
          ['value' => 'timestamp'],
          'or',
          ['value' => 'iso8601'],
        ],
      ],
    ];

    // Filter rank.
    $form['filter_rank'] = $this->getNumericFilterForm($params);
    $form['filter_rank']['#type'] = 'fieldset';
    $form['filter_rank']['#title'] = $this->t('Filter rank');
    $form['filter_rank']['#description'] = $this->t('Use rank filtering if needed when you have multiple values for the same value type.');

    return $form;
  }

  /**
   * Returns SQL conditions to filter a property table.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $join_name
   *   The table alias used in the join expression.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getChadoTablePropFilterConditions(
    array $settings,
    string $join_name
  ) :array {

    $conditions = [];

    // Filter type_id.
    $conditions = array_merge(
      $conditions,
      $this->getChadoCvtermFilterConditions($settings['filter_type_id'], $join_name . '.type_id')
    );

    // Filter value.
    switch ($settings['filter_value']['filter_value_mode']) {
      case 'text':
        $conditions = array_merge(
          $conditions,
          $this->getTextFilterConditions($settings['filter_value']['filter_value_text'], $join_name . '.value')
        );
        break;

      case 'numeric':
        $conditions = array_merge(
          $conditions,
          $this->getNumericFilterConditions($settings['filter_value']['filter_value_numeric'], $join_name . '.value')
        );
        break;

      case 'timestamp':
        $conditions = array_merge(
          $conditions,
          $this->getDateTimestampFilterConditions($settings['filter_value']['filter_value_date'], $join_name . '.value')
        );
        break;

      case 'iso8601':
        $conditions = array_merge(
          $conditions,
          $this->getDateIso8601FilterConditions($settings['filter_value']['filter_value_date'], $join_name . '.value')
        );
        break;

      default:
        break;
    }

    // Filter rank.
    $conditions = array_merge(
      $conditions,
      $this->getNumericFilterConditions($settings['filter_rank'], $join_name . '.rank')
    );

    return $conditions;
  }

  /**
   * Returns a CV Term table filter form.
   *
   * @param array $params
   *   An associative array of parameters. See self::getChadoCvtermFilterForm().
   *
   * @return array
   *   A Drupal form array with table cvterm filtering.
   */
  public function getChadoTableCvtermFilterForm(
    array $params = []
  ) :array {
    $form = [
      '#type' => 'fieldset',
      '#title' => $this->t('Controlled vocabulary filtering'),
    ];

    // Filter cvterm_id.
    $form['filter_term'] = $this->getChadoCvtermFilterForm($params);
    $form['filter_term']['#type'] = 'fieldset';
    $form['filter_term']['#title'] = $this->t('Filter terms');

    // Filter rank.
    $form['filter_rank'] = $this->getNumericFilterForm($params);
    $form['filter_rank']['#type'] = 'fieldset';
    $form['filter_rank']['#title'] = $this->t('Filter rank');
    $form['filter_rank']['#description'] = $this->t('Use rank filtering if needed when you have multiple values for the same value type.');

    // Filter is_not.
    $form['filter_neg'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter negative annotation ("is_not")'),
    ];
    $form['filter_neg']['filter_value'] = [
      '#type' => 'radios',
      '#options' => [
        '' => $this->t('Ignore'),
        'is' => $this->t('Is (the given term)'),
        'isnot' => $this->t('Is NOT (the given term)'),
      ],
      '#default_value' => '',
    ];

    return $form;
  }

  /**
   * Returns conditions to filter a CV Term table.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $join_name
   *   The table alias used in the join expression.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getChadoTableCvtermFilterConditions(
    array $settings,
    string $join_name
  ) :array {

    $conditions = [];

    // Filter cvterm_id.
    $conditions = array_merge(
      $conditions,
      $this->getChadoCvtermFilterConditions($settings['filter_term'], $join_name . '.cvterm_id')
    );

    // Filter rank.
    $conditions = array_merge(
      $conditions,
      $this->getNumericFilterConditions($settings['filter_rank'], $join_name . '.rank')
    );

    // Filter is_not.
    switch ($settings['filter_neg']['filter_value']) {
      case 'is':
        $conditions[] = $join_name . '.is_not = FALSE';
        break;

      case 'isnot':
        $conditions[] = $join_name . '.is_not = TRUE';
        break;

      default:
        break;
    }

    return $conditions;
  }

  /**
   * Returns a DBXRef table filter form.
   *
   * @param array $params
   *   An associative array of parameters. See self::getChadoDbxrefFilterForm().
   *
   * @return array
   *   A Drupal form array with table dbxref filtering.
   */
  public function getChadoTableDbxrefFilterForm(
    array $params = []
  ) :array {
    $form = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cross-reference filtering'),
    ];

    $form['filter_dbxref'] = $this->getChadoDbxrefFilterForm($params);

    $form['filter_current'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter on current value status ("is_current")'),
    ];
    $form['filter_current']['filter_value'] = [
      '#type' => 'radios',
      '#options' => [
        '' => $this->t('Ignore'),
        'current' => $this->t('Must be current value'),
        'past' => $this->t('Must be a past value'),
      ],
      '#default_value' => '',
    ];

    return $form;
  }

  /**
   * Returns SQL conditions to filter a DBXRef table.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $join_name
   *   The table alias used in the join expression.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getChadoTableDbxrefFilterConditions(
    array $settings,
    string $join_name
  ) :array {

    $conditions = [];

    // Filter dbxref_id.
    $conditions = array_merge(
      $conditions,
      $this->getChadoDbxrefFilterConditions($settings['filter_dbxref'], $join_name . '.dbxref_id')
    );

    // Filter is_current.
    switch ($settings['filter_current']['filter_value']) {
      case 'current':
        $conditions[] = $join_name . '.is_current = TRUE';
        break;

      case 'past':
        $conditions[] = $join_name . '.is_current = FALSE';
        break;

      default:
        break;
    }

    return $conditions;
  }

  /**
   * Returns a publication table filter form.
   *
   * @param array $params
   *   An associative array of parameters. See self::getChadoCvtermFilterForm().
   *
   * @return array
   *   A Drupal form array with table Publication filtering.
   */
  public function getChadoTablePubFilterForm(
    array $params = []
  ) :array {
    $form = [
      '#type' => 'container',
    ];

    // Publication type.
    $form['filter_type_id'] = $this->getChadoCvtermFilterForm($params);
    $form['filter_type_id']['#type'] = 'fieldset';
    $form['filter_type_id']['#title'] = $this->t('Filter publication type');
    $form['filter_type_id']['filter_mode']['#title'] = $this->t('Filter type: only terms from...');

    // Publication fields.
    $pub_fields = [
      'title' => $this->t('Publication title'),
      'volumetitle' => $this->t('Volume title of part if one of a series'),
      'volume' => $this->t('Volume'),
      'series_name' => $this->t('Full name of (journal) series'),
      'issue' => $this->t('Issue'),
      'pyear' => $this->t('Publication year'),
      'miniref' => $this->t('Mini-reference'),
      'publisher' => $this->t('Publisher'),
      'pubplace' => $this->t('Publication place'),
      'uniquename' => $this->t('Entry unique name'),
    ];
    foreach ($pub_fields as $pub_field => $label) {
      $form['filter_' . $pub_field] = $this->getTextFilterForm();
      $form['filter_' . $pub_field]['#type'] = 'fieldset';
      $form['filter_' . $pub_field]['#title'] = $this->t(
        'Filter on "@field_label"',
        ['@field_label' => $label]
      );
    }

    // Obsolete status.
    $form['filter_obsolete'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter obsolescence status ("is_obsolete")'),
    ];
    $form['filter_obsolete']['filter_value'] = [
      '#type' => 'radios',
      '#options' => [
        '' => $this->t('Ignore'),
        'current' => $this->t('Is current'),
        'obsolete' => $this->t('Is obsolete'),
      ],
      '#default_value' => '',
    ];

    return $form;
  }

  /**
   * Returns SQL conditions to filter a Pub table.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $join_name
   *   The table alias used in the join expression.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getChadoTablePubFilterConditions(
    array $settings,
    string $join_name
  ) :array {

    $conditions = [];

    // Filter type_id.
    $conditions = array_merge(
      $conditions,
      $this->getChadoCvtermFilterConditions($settings['filter_type_id'], $join_name . '.type_id')
    );

    // Publication fields.
    $pub_fields = [
      'title' => $this->t('Publication title'),
      'volumetitle' => $this->t('Volume title of part if one of a series'),
      'volume' => $this->t('Volume'),
      'series_name' => $this->t('Full name of (journal) series'),
      'issue' => $this->t('Issue'),
      'pyear' => $this->t('Publication year'),
      'miniref' => $this->t('Mini-reference'),
      'publisher' => $this->t('Publisher'),
      'pubplace' => $this->t('Publication place'),
      'uniquename' => $this->t('Entry unique name'),
    ];
    foreach ($pub_fields as $pub_field => $label) {
      $this->getTextFilterConditions(
        $settings['filter_' . $pub_field],
        $join_name . '.' . $pub_field
      );
    }

    // Filter is_obsolete.
    switch ($settings['filter_obsolete']['filter_value']) {
      case 'current':
        $conditions[] = $join_name . '.is_obsolete = FALSE';
        break;

      case 'obsolete':
        $conditions[] = $join_name . '.is_obsolete = TRUE';
        break;

      default:
        break;
    }

    return $conditions;
  }

  /**
   * Returns a relationship table filter form.
   *
   * @param array $params
   *   An associative array of parameters. See self::getChadoCvtermFilterForm().
   *
   * @return array
   *   A Drupal form array with table relationship filtering.
   */
  public function getChadoTableRelationshipFilterForm(
    array $params = []
  ) :array {
    $form = [
      '#type' => 'fieldset',
      '#title' => $this->t('Relationship filtering'),
      // @todo Add link to tab.
      '#description' => $this->t(
        'You can manage the related content display using the corresponding field in "@manage_display".',
        [
          '@manage_display' => $this->t('Manage display'),
        ]
      ),
    ];
    $form['filter_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Consider current content as...'),
      '#options' => [
        'subject' => $this->t('... relationship subject'),
        'object' => $this->t('... relationship object'),
        'any' => $this->t('... any of subject or object'),
      ],
      '#default_value' => 'subject',
    ];

    $form['filter_type_id'] = $this->getChadoCvtermFilterForm($params);
    $form['filter_type_id']['#type'] = 'fieldset';
    $form['filter_type_id']['#title'] = $this->t('Filter relationship type');
    $form['filter_type_id']['filter_mode']['#title'] = $this->t('Filter type: only terms from...');

    return $form;
  }

  /**
   * Returns SQL conditions to filter a Relationship table.
   *
   * @param array $settings
   *   The filtering settings.
   * @param string $join_name
   *   The table alias used in the join expression.
   * @param string $source_id_alias
   *   The SQL alias used for the source identifier.
   *
   * @return array
   *   An array of SQL expressions.
   */
  public function getChadoTableRelationshipFilterConditions(
    array $settings,
    string $join_name,
    string $source_id_alias
  ) :array {

    $conditions = [];

    // Filter relationship mode.
    switch ($settings['filter_mode']) {
      case 'subject':
        $conditions[] = $join_name . '.subject_id = ' . $source_id_alias;
        break;

      case 'object':
        $conditions[] = $join_name . '.object_id = ' . $source_id_alias;
        break;

      case 'any':
        $conditions[] = '(' . $join_name . '.subject_id = ' . $source_id_alias . ' OR ' . $join_name . '.object_id = ' . $source_id_alias . ')';
        break;

      default:
        break;
    }

    // Filter type_id.
    $conditions = array_merge(
      $conditions,
      $this->getChadoCvtermFilterConditions($settings['filter_type_id'], $join_name . '.type_id')
    );

    return $conditions;
  }

}
