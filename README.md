Chado Light 
===========

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

Chado is a PostgreSQL database schema for biological data. This extension
provides simplified access to Chado data from Drupal.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/chadol

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/chadol

REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.
 * If you enabled the module using Drush, you must also rebuild the cache.

CONFIGURATION
-------------

The module has no global menu or modifiable settings. It is a storage client
plugin that can be used in an external entity type. It is better to use this
client as a single storage client as it automatically add fields to the
external entity type and map those. However, it is still possible to add custom
fields as well and use extra storage clients with a data aggregator but keep in
mind automatic mapping may change your custom mapping on Chado Light fields.

TROUBLESHOOTING
---------------

 * Things are no working as expected:

   - Do you have an error message ?
     If so, check there if nobody else had a similar problem:
     
       https://www.drupal.org/project/issues/chadol?categories=All

     and if not, please report your issue (Create issue) as "Support request".

   - No error message ?
     There should be one either in your Drupal site logs (/admin/reports/dblog)
     or in your web server logs or in your PHP interpreter (ie. fpm) logs.
     If not, it will be really complicated to help you. Try so summarize the
     steps that led you to your problem and post an issue. If your problem is
     reproductible then, there are chances you can get some help.

   - The more information you provide, the better your chances are to get
     support. Include your Drupal version, your PHP version, your PostgreSQL
     server version, how you instanciated your Chado schema, what type of data
     it contains (tables that contain data other than CV* and DBX*), how did you
     you load it, what did you do with Chado Light and so on.

FAQ
---

Q: How do I add a Chado instance?

A: It should be added in your Drupal site settings.php file in the $databases
  array. For instance:
  $databases['my_extra_chado']['default'] = [
    'database' => 'some_chado_database',
    'username' => 'your_db_user',
    'password' => 'your_db_password',
    'prefix' => '',
    'host' => 'some_ip_or_name_if_not_drupal_server',
    'port' => '5432',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\pgsql',
    'driver' => 'pgsql',
  ];

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
